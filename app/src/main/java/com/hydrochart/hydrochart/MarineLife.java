package com.hydrochart.hydrochart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowInsets;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hydrochart.hydrochart.Databases.DetailsDatabase;
import com.hydrochart.hydrochart.Databases.MarineLifeDraft;
import com.hydrochart.hydrochart.Databases.PublicationDraft;
import com.hydrochart.hydrochart.Databases.portinformationDraft;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class MarineLife extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener{


    private static final int ADDRESS_PICKER_REQUEST = 1000;
    Toolbar toolbar;
    Button MARINELIFECamera,MARINELIFEGallery, MARINELIFElocationPicker,MARINELIFESubmit;
    private RecyclerView recyclerView;
    List<String> MARINELIFEfileURI;
    List<String> MARINELIFEdownloadFilePath;
    Recycler MARINELIFErecycler;
    String MARINELIFEcurrentPhotoPath;
    DetailsDatabase MARINELIFEdatabase;
    private Spinner MARINELIFE_reporting,MARINELIFE_confidence,MARINELIFE_range;
    private Spinner MARINELIFEspinner_latitude,MARINELIFEspinner_longtitude;

    public TextInputLayout MARINELIFESpecies,MARINELIFENumberObserved,MARINELIFELatitudeDegree,MARINELIFELatitudeMinutes, MARINELIFELatitudeDecimal;
    TextInputLayout MARINELIFELongitudeDegrees, MARINELIFELongitudeMinutes,MARINELIFELongitudeDecimal,MARINELIFESupportingInformation;
    TextInputLayout MARINELIFELength,MARINELIFEBehaviour,MARINELIFEMeteorological,MARINELIFEBearing;

    String marinespecies,marinennumberobserved,marinelatitudedegree,marinelatitudeminutes,marinelatitudedecimal,marinelongitudedegrees,marinelongtitugeminutes,marinelongitudedecimal;
    String marinelatitudepostion,marinelongitudeposition;

    String marinereportingspinner,marineconfidencespinner,marinelength,marinebehaviour,marinedate,marinetime,marinemonitoring,marinemeteorological,marinerangespinner,marinebearing;

    String montioring;

    String userEmail,userName,userCompany,userOccupation,userHomeCountry,userVesselName,userIMONumber;
    RadioGroup marineradioGroup;
    RadioButton marinlifeAcoustic,marinelifeVisual;
    String downloadPath;
    String saveCurrentDate,saveCurrentTime,RandomKEY;
    String marinesupportinginformation;
    private ProgressDialog Progress;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int GALLERY_REQUEST_CODE = 105;
    // private StorageReference mStorage;
    private StorageReference marineImageRef;
    private DatabaseReference marinedataRef;


    TextInputEditText marinelifeDate,
            marinelifeTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marine_life);

        toolbar = findViewById(R.id.toolbarenc);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        marinelifeDate=findViewById(R.id.marinelifeDate);
        marinelifeTime=findViewById(R.id.marinelifeTime);

        marinelifeVisual=findViewById(R.id.marinelifeVisual);
        marinlifeAcoustic=findViewById(R.id.marinelifeAcoustic);


        MARINELIFECamera = findViewById(R.id. marinelifeCamera);
        MARINELIFEGallery = findViewById(R.id. marinelifeGallary);
        MARINELIFElocationPicker = findViewById(R.id. marinelifeUseLocationPicker);
        MARINELIFESubmit=findViewById(R.id.submitmarinelifeDetails);
        recyclerView = findViewById(R.id. marinelifeRecycler);
        MARINELIFEfileURI = new ArrayList<>();
        MARINELIFErecycler = new Recycler(this,    MARINELIFEfileURI);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(  MARINELIFErecycler);
        //   mStorage = FirebaseStorage.getInstance().getReference();
        MARINELIFEdownloadFilePath=new ArrayList<>();
        marineImageRef = FirebaseStorage.getInstance().getReference().child("Images");
       marinedataRef = FirebaseDatabase.getInstance().getReference().child("MarineLifeDetails");
        Progress = new ProgressDialog(this);
        MapUtility.apiKey = getResources().getString(R.string.your_api_key);
        MARINELIFEdatabase = Room.databaseBuilder(this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();


        MARINELIFE_reporting=findViewById(R.id.marinelife_reporting);
        MARINELIFE_confidence=findViewById(R.id.marinelife_confidence);
        MARINELIFE_range=findViewById(R.id.marinelife_range);
        MARINELIFEspinner_latitude=findViewById(R.id.marinelifespinner_latitude);
        MARINELIFEspinner_longtitude=findViewById(R.id.marinelifespinner_longitude);

        MARINELIFESpecies=findViewById(R.id.marinelifeSpecies);
        MARINELIFENumberObserved=findViewById(R.id.marinelifeNumberObserved);
        MARINELIFELatitudeDegree=findViewById(R.id.marinelifeLatitudeDegree);
        MARINELIFELatitudeMinutes=findViewById(R.id.marinelifeLatitudeMinutes);
        MARINELIFELatitudeDecimal=findViewById(R.id.marinelifeLatitudeDecimal);
        MARINELIFELongitudeDegrees=findViewById(R.id.marinelifeLongitudeDegrees);
        MARINELIFELongitudeMinutes=findViewById(R.id.marinelifeLongitudeMinutes);
        MARINELIFELongitudeDecimal=findViewById(R.id.marinelifeLongitudeDecimal);
        MARINELIFESupportingInformation=findViewById(R.id.marinelifeSupportingInformation);
        MARINELIFELength=findViewById(R.id.marinelifeLength);
        MARINELIFEBehaviour=findViewById(R.id.marinelifeBehavior);
        MARINELIFEMeteorological=findViewById(R.id.marinelifeMeteorological);
        MARINELIFEBearing=findViewById(R.id.marinelifeBearing);

        marineradioGroup=findViewById(R.id.marineliferadioGroup);


        marineradioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == R.id.marinelifeVisual) {
                    marinemonitoring="Visual";

                }
                if (group.getCheckedRadioButtonId() == R.id.marinelifeAcoustic) {
                    marinemonitoring="Acoustic";
                }
            }
        });

        marinemonitoring="Visual";
        marinelifeVisual.setChecked(true);


        List<MarineLifeDraft> marineLifeDrafts=new ArrayList<>();

        marineLifeDrafts.addAll(MARINELIFEdatabase.getMarineLifeDraft().getMarineLifeDraft());

        userName =   MARINELIFEdatabase.getDetails().getDetails().get(0).getUserName();
        userEmail=     MARINELIFEdatabase.getDetails().getDetails().get(0).getEmail();
        userCompany =   MARINELIFEdatabase.getDetails().getDetails().get(0).getCompany();
        userOccupation = MARINELIFEdatabase.getDetails().getDetails().get(0).getOccupation();
        userHomeCountry=  MARINELIFEdatabase.getDetails().getDetails().get(0).getHomeCountry();
        userVesselName=MARINELIFEdatabase.getDetails().getDetails().get(0).getVesselName();
        userIMONumber= MARINELIFEdatabase.getDetails().getDetails().get(0).getImoNumber();


        MARINELIFESubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             checkChartInput();
            }
        });

        MARINELIFECamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               askCameraPermissions();

            }
        });


        ArrayAdapter<CharSequence> reporting = ArrayAdapter.createFromResource(this,R.array.Reporting,android.R.layout.simple_spinner_item);

        reporting.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        MARINELIFE_reporting.setAdapter(reporting);
        MARINELIFE_reporting.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        marinereportingspinner="";
                        break;
                    case 1:
                        marinereportingspinner="Whales & Dolphins";
                        break;
                    case 2:
                        marinereportingspinner="Fish Including Sharks";
                        break;
                    case 3:
                        marinereportingspinner="Sea Snakes";
                        break;
                    case 4:
                        marinereportingspinner="Seals & Sea Lions";
                        break;
                    case 5:
                        marinereportingspinner="Turtles";
                        break;
                    case 6:
                        marinereportingspinner="Others";
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<CharSequence> confidence = ArrayAdapter.createFromResource(this,R.array.Confidence,android.R.layout.simple_spinner_item);

        confidence.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        MARINELIFE_confidence.setAdapter(confidence);
        MARINELIFE_confidence.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        marineconfidencespinner="";
                        break;
                    case 1:
                        marineconfidencespinner="Certain";
                        break;
                    case 2:
                        marineconfidencespinner="Possible";
                        break;
                    case 3:
                        marineconfidencespinner="Unsure";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> range = ArrayAdapter.createFromResource(this,R.array.Range,android.R.layout.simple_spinner_item);

        confidence.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        MARINELIFE_range.setAdapter(range);
        MARINELIFE_range.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        marinerangespinner="";
                        break;
                    case 1:
                        marinerangespinner="0m-100m";
                        break;
                    case 2:
                        marinerangespinner="200m-500m";
                        break;
                    case 3:
                        marinerangespinner=">1000m";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        MARINELIFEGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
            }
        });


        MARINELIFElocationPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MarineLife.this, LocationPickerActivity.class);
                startActivityForResult(i, ADDRESS_PICKER_REQUEST);
            }
        });


      marinelifeDate.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              DialogFragment datePicker = new DatePickerFragment();
              datePicker.show(getSupportFragmentManager(), "date picker");

          }
      });

      marinelifeTime.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              DialogFragment timePicker = new TimePickerFragment();
              timePicker.show(getSupportFragmentManager(), "time picker");
          }
      });


        ArrayAdapter<CharSequence> latitude = ArrayAdapter.createFromResource(this,R.array.NS,android.R.layout.simple_spinner_item);

        latitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        MARINELIFEspinner_latitude.setAdapter(latitude);
        MARINELIFEspinner_latitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        marinelatitudepostion="N";
                        break;
                    case 1:
                        marinelatitudepostion="S";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> longitude = ArrayAdapter.createFromResource(this,R.array.EW,android.R.layout.simple_spinner_item);

        longitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        MARINELIFEspinner_longtitude.setAdapter(longitude);
        MARINELIFEspinner_longtitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        marinelongitudeposition="E";
                        break;
                    case 1:
                        marinelongitudeposition="W";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Intent intent = getIntent();

        String publicationdraft="NO";
        int selector=intent.getIntExtra("MARINELIFEDraft",0);

        //Toast.makeText(this, chartdraft+"", Toast.LENGTH_SHORT).show();
        if(selector==1)
        {

            if(marineLifeDrafts.size()>0)
            {

                if(marineLifeDrafts.get(0).getMonitoring().equals("Visual"))
                {
                    marinelifeVisual.setChecked(true);
                }
                else {
                    marinlifeAcoustic.setChecked(true);
                }


            //Toast.makeText(this, "DONEEE"+"sdadasd"+database.getChart().getChartDraft().get(0).getLatitudeDecimal(), Toast.LENGTH_SHORT).show();
           MARINELIFESpecies.getEditText().setText(marineLifeDrafts.get(0).getSpecies());
           MARINELIFENumberObserved.getEditText().setText(marineLifeDrafts.get(0).getNameObserved());
           MARINELIFELength.getEditText().setText((marineLifeDrafts.get(0).getLength()));

           MARINELIFEBehaviour.getEditText().setText(marineLifeDrafts.get(0).getBehaviour());
           marinelifeTime.setText(marineLifeDrafts.get(0).getTime());
            marinelifeDate.setText(marineLifeDrafts.get(0).getDate());
            MARINELIFEMeteorological.getEditText().setText(marineLifeDrafts.get(0).getMeteorological());
            MARINELIFEBearing.getEditText().setText(marineLifeDrafts.get(0).getBearing());

            MARINELIFELatitudeDegree.getEditText().setText(marineLifeDrafts.get(0).getLatitudeDegree());
            MARINELIFELatitudeMinutes.getEditText().setText(marineLifeDrafts.get(0).getLatitudeMinutes());
            MARINELIFELatitudeDecimal.getEditText().setText(marineLifeDrafts.get(0).getLatitudeDecimal());
            MARINELIFE_reporting.setSelection(Integer.parseInt(marineLifeDrafts.get(0).getReportingselectposition()));
            MARINELIFE_confidence.setSelection(Integer.parseInt(marineLifeDrafts.get(0).getConfidenceselectposition()));
            MARINELIFE_range.setSelection(Integer.parseInt(marineLifeDrafts.get(0).getRangePosition()));

            String latitudeP=marineLifeDrafts.get(0).getLatitudePosition();

         //   Toast.makeText(this, latitudeP+"", Toast.LENGTH_SHORT).show();
            if(latitudeP.equals("N"))
            {
                //  Toast.makeText(this, "N", Toast.LENGTH_SHORT).show();
                MARINELIFEspinner_latitude.setSelection(0);
            }
            if(latitudeP.equals("S")){
                //  Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                MARINELIFEspinner_latitude.setSelection(1);
            }
            MARINELIFELongitudeDegrees.getEditText().setText(marineLifeDrafts.get(0).getLongitudeDegree());
            MARINELIFELongitudeMinutes.getEditText().setText(marineLifeDrafts.get(0).getLongitudeMinutes());
            MARINELIFELongitudeDecimal.getEditText().setText(marineLifeDrafts.get(0).getLongitudeDecimal());
            String longitudeP=marineLifeDrafts.get(0).getLongitudePosition();
          //  Toast.makeText(this, longitudeP+"", Toast.LENGTH_SHORT).show();

            if(longitudeP.equals("E"))
            {
                //     Toast.makeText(this, "E", Toast.LENGTH_SHORT).show();
                MARINELIFEspinner_longtitude.setSelection(0);
            }
            if(longitudeP.equals("W")){
                //    Toast.makeText(this, "W", Toast.LENGTH_SHORT).show();
                MARINELIFEspinner_longtitude.setSelection(1);
            }


            MARINELIFESupportingInformation.getEditText().setText(marineLifeDrafts.get(0).getObservation());
            for(int i=0;i<marineLifeDrafts.get(0).getURLIMAGE().size();i++)
            {

                if(!marineLifeDrafts.get(0).getURLIMAGE().get(i).contains("content:"))
                {
                    MARINELIFEfileURI.add(marineLifeDrafts.get(0).getURLIMAGE().get(i));
                }
                Log.e("img",marineLifeDrafts.get(0).getURLIMAGE().get(i));

            }
            MARINELIFErecycler.notifyDataSetChanged();

        }
        }

    }


    private void checkChartInput() {

        if(!validateMARINELIFELength()|!validateMARINELIFESpecies()|!validateMARINELIFENumberObserved()|!validateMARINELIFELatitudeDegree()|!validateMARINELIFELatitudeMinutes()|!validateMARINELIFELatitudeDecimal()|!validateMARINELIFELongitudeDegree()|!validateMARINELIFELongitudeMinutes()|!validateMARINELIFELongitudeDecimal())
        {
            return;
        }

        marinesupportinginformation =     MARINELIFESupportingInformation.getEditText().getText().toString().trim();
        marinebehaviour=MARINELIFEBehaviour.getEditText().getText().toString().trim();
       marinedate=marinelifeDate.getText().toString().trim();
       marinetime=marinelifeTime.getText().toString().trim();
       marinemeteorological=MARINELIFEMeteorological.getEditText().getText().toString().trim();
       marinebearing=MARINELIFEBearing.getEditText().getText().toString().trim();



    //    Toast.makeText(this, "DONE", Toast.LENGTH_SHORT).show();
        Progress.setTitle("Uploading Information..");
        Progress.setMessage("PLEASE WAIT WE ARE UPLOADING INFORMATION..");
        Progress.setCanceledOnTouchOutside(false);
        Progress.show();
        SaveData();
    }


    private void SaveData() {
        MARINELIFEdownloadFilePath.clear();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a"); // it will give us the currentTime a means am and pm
        saveCurrentTime = currentTime.format(calendar.getTime());
        RandomKEY = saveCurrentDate + saveCurrentTime;
        if (    MARINELIFEfileURI.size() > 0) {
            for (int i = 0; i <     MARINELIFEfileURI.size(); i++) {
                final StorageReference filePath;
                filePath =  marineImageRef.child(Uri.parse(    MARINELIFEfileURI.get(i)).getLastPathSegment() + RandomKEY + ".jpg"); //last path segment is used to get the imageuri last / part which is basically image name in the uri
                final UploadTask uploadTask = filePath.putFile(Uri.parse(    MARINELIFEfileURI.get(i)));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String messege = e.toString();
                        Progress.dismiss();
                        Toast.makeText(MarineLife.this, "Error: " + messege + "..", Toast.LENGTH_SHORT).show();

                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { // tasksnapshot show about the state of the running upload task
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //       Toast.makeText(ChartActivity.this, "Image Stored Successfully..", Toast.LENGTH_SHORT).show();

                        Task<Uri> taskUrl = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {

                                    throw task.getException();
                                }
                                downloadPath = filePath.getDownloadUrl().toString();

                                return filePath.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {

                                    downloadPath = task.getResult().toString();
                                    Log.e("image", downloadPath);
                                    //Toast.makeText(ChartActivity.this, downloadPath + "", Toast.LENGTH_SHORT).show();
                                    MARINELIFEdownloadFilePath.add(downloadPath);
                                    // Toast.makeText(ChartActivity.this, "Product Image URL Got Successfully..", Toast.LENGTH_SHORT).show();
                                    // SaveProductInfoToDatabase();
                                    Log.e("TAGINGINGIG",    MARINELIFEdownloadFilePath.size()+"  "+     MARINELIFEfileURI.size());
                                    if (    MARINELIFEdownloadFilePath.size() ==     MARINELIFEfileURI.size()) {
                                        SaveProductInfoToPORTINFORMATION();
                                    }
                                }
                            }
                        });
                    }
                });

            }
            Log.e("calling", "work");

            //    SaveProductInfoToChart();
        }

        else
        {

            HashMap<String ,Object> hashMap= new HashMap<>();
            // hashMap.put("image",downloadFilePath.get(0));
            hashMap.put("UserName",userName);
            hashMap.put("UserEmail",userEmail);
            hashMap.put("UserCompany",userCompany);
            hashMap.put("UserOccupation",userOccupation);
            hashMap.put("UserHomeCountry",userHomeCountry);
            hashMap.put("UserVesselName",userVesselName);
            hashMap.put("UserIMONumber",userIMONumber);
            hashMap.put("MARINELIFELatitudeDegree",marinelatitudedegree);
            hashMap.put("MARINELIFELatitudeMinutes",marinelatitudeminutes);
            hashMap.put("MARINELIFELatitudeDecimals",marinelatitudedecimal);
            hashMap.put("MARINELIFELatitudePosition",marinelatitudepostion);
            hashMap.put("MARINELIFELongitudeDegree",marinelongitudedegrees);
            hashMap.put("MARINELIFELongitudeMinutes",marinelongtitugeminutes);
            hashMap.put("MARINELIFELongitudeDecimals",marinelongitudedecimal);
            hashMap.put("MARINELIFELongitudePosition",marinelongitudeposition);
            hashMap.put("MARINELIFESupportingInformation",marinesupportinginformation);
            hashMap.put("MARINELIFEReportingInfo",marinereportingspinner);
            hashMap.put("MARINELIFESpecies",marinespecies);
            hashMap.put("MARINELIFEConfidenceInfo",marineconfidencespinner);
            hashMap.put("MARINELIFENumberObserved",marinennumberobserved);
            hashMap.put("MARINELIFELength",marinelength);
            hashMap.put("MARINELIFEBehaviour",marinebehaviour);
            hashMap.put("MARINELIFEDate",marinedate);
            hashMap.put("MARINELIFETime",marinetime);
            hashMap.put("MARINELIFEMonitoringInfo",marinemonitoring);
            hashMap.put("MARINELIFEMeteorological",marinemeteorological);
            hashMap.put("MARINELIFERange",marinerangespinner);
            hashMap.put("MARINELIFEBearing",marinebearing);
            hashMap.put("date",saveCurrentDate);
            hashMap.put("time",saveCurrentTime);






           marinedataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {

                        Toast.makeText(MarineLife.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                        Progress.dismiss();
                    }
                    else {
                        //   loginProgress.dismiss();
                        String messege =task.getException().toString();
                        Progress.dismiss();
                        Toast.makeText(MarineLife.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


    }


    private void SaveProductInfoToPORTINFORMATION() {
        HashMap<String ,Object> hashMap= new HashMap<>();
        // hashMap.put("image",downloadFilePath.get(0));
        for(int i=0;i<    MARINELIFEdownloadFilePath.size();i++)
        {
            hashMap.put("image"+i,MARINELIFEdownloadFilePath.get(i));
        }
        hashMap.put("UserName",userName);
        hashMap.put("UserEmail",userEmail);
        hashMap.put("UserCompany",userCompany);
        hashMap.put("UserOccupation",userOccupation);
        hashMap.put("UserHomeCountry",userHomeCountry);
        hashMap.put("UserVesselName",userVesselName);
        hashMap.put("UserIMONumber",userIMONumber);
        hashMap.put("MARINELIFELatitudeDegree",marinelatitudedegree);
        hashMap.put("MARINELIFELatitudeMinutes",marinelatitudeminutes);
        hashMap.put("MARINELIFELatitudeDecimals",marinelatitudedecimal);
        hashMap.put("MARINELIFELatitudePosition",marinelatitudepostion);
        hashMap.put("MARINELIFELongitudeDegree",marinelongitudedegrees);
        hashMap.put("MARINELIFELongitudeMinutes",marinelongtitugeminutes);
        hashMap.put("MARINELIFELongitudeDecimals",marinelongitudedecimal);
        hashMap.put("MARINELIFELongitudePosition",marinelongitudeposition);
        hashMap.put("MARINELIFESupportingInformation",marinesupportinginformation);
        hashMap.put("MARINELIFEReportingInfo",marinereportingspinner);
        hashMap.put("MARINELIFESpecies",marinespecies);
        hashMap.put("MARINELIFEConfidenceInfo",marineconfidencespinner);
        hashMap.put("MARINELIFENumberObserved",marinennumberobserved);
        hashMap.put("MARINELIFELength",marinelength);
        hashMap.put("MARINELIFEBehaviour",marinebehaviour);
        hashMap.put("MARINELIFEDate",marinedate);
        hashMap.put("MARINELIFETime",marinedate);
        hashMap.put("MARINELIFEMonitoringInfo",marinemonitoring);
        hashMap.put("MARINELIFEMeteorological",marinemeteorological);
        hashMap.put("MARINELIFERange",marinerangespinner);
        hashMap.put("MARINELIFEBearing",marinebearing);
        hashMap.put("date",saveCurrentDate);
        hashMap.put("time",saveCurrentTime);










        marinedataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Progress.dismiss();
                    Toast.makeText(MarineLife.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    //   loginProgress.dismiss();
                    Progress.dismiss();
                    String messege =task.getException().toString();
                    Toast.makeText(MarineLife.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERM_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        MARINELIFEdatabase = Room.databaseBuilder(MarineLife.this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

                        List<MarineLifeDraft> marineLifeDrafts = new ArrayList<>();

                        marineLifeDrafts.addAll(MARINELIFEdatabase.getMarineLifeDraft().getMarineLifeDraft());

                        //   Log.e("Tag",draftList.get(0).URLIMAGE.get(0));
                        if(marineLifeDrafts.size()>0) {

                          //  Toast.makeText(MarineLife.this, "UPDATE", Toast.LENGTH_SHORT).show();
                            MarineLifeDraft marineLifeDraft = new MarineLifeDraft(0,String.valueOf(MARINELIFE_reporting.getSelectedItemPosition()),MARINELIFESpecies.getEditText().getText().toString(),String.valueOf(MARINELIFE_confidence.getSelectedItemPosition()),MARINELIFENumberObserved.getEditText().getText().toString(),MARINELIFELength.getEditText().getText().toString(),MARINELIFEBehaviour.getEditText().getText().toString(),marinelifeDate.getText().toString(),marinelifeTime.getText().toString(),MARINELIFELatitudeDegree.getEditText().getText().toString(),MARINELIFELatitudeMinutes.getEditText().getText().toString(),MARINELIFELatitudeDecimal.getEditText().getText().toString(),marinelatitudepostion,MARINELIFELongitudeDegrees.getEditText().getText().toString(),MARINELIFELongitudeMinutes.getEditText().getText().toString(),MARINELIFELongitudeDecimal.getEditText().getText().toString(),marinelongitudeposition,marinemonitoring,MARINELIFEMeteorological.getEditText().getText().toString(),String.valueOf(MARINELIFE_range.getSelectedItemPosition()),MARINELIFEBearing.getEditText().getText().toString(),MARINELIFESupportingInformation.getEditText().getText().toString(),MARINELIFEfileURI);

                            MARINELIFEdatabase.getMarineLifeDraft().updateMarineLifeDraft(marineLifeDraft);
                        }
                        else
                        {

                            MarineLifeDraft marineLifeDraft = new MarineLifeDraft(0,String.valueOf(MARINELIFE_reporting.getSelectedItemPosition()),MARINELIFESpecies.getEditText().getText().toString(),String.valueOf(MARINELIFE_confidence.getSelectedItemPosition()),MARINELIFENumberObserved.getEditText().getText().toString(),MARINELIFELength.getEditText().getText().toString(),MARINELIFEBehaviour.getEditText().getText().toString(),marinelifeDate.getText().toString(),marinelifeTime.getText().toString(),MARINELIFELatitudeDegree.getEditText().getText().toString(),MARINELIFELatitudeMinutes.getEditText().getText().toString(),MARINELIFELatitudeDecimal.getEditText().getText().toString(),marinelatitudepostion,MARINELIFELongitudeDegrees.getEditText().getText().toString(),MARINELIFELongitudeMinutes.getEditText().getText().toString(),MARINELIFELongitudeDecimal.getEditText().getText().toString(),marinelongitudeposition,marinemonitoring,MARINELIFEMeteorological.getEditText().getText().toString(),String.valueOf(MARINELIFE_range.getSelectedItemPosition()),MARINELIFEBearing.getEditText().getText().toString(),MARINELIFESupportingInformation.getEditText().getText().toString(),MARINELIFEfileURI);
                            MARINELIFEdatabase.getMarineLifeDraft().addMarineLifeDraft(marineLifeDraft);
                        }



                        finish();
                        //Yes button clicked
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked


                        finish();
                        break;
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(MarineLife.this);
        builder.setMessage("Would You Like To Save Draft?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(MARINELIFEcurrentPhotoPath);
                //selectedImage.setImageURI(Uri.fromFile(f));
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                //  f.getAbsolutePath();
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                Log.e("api", contentUri.toString());

                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                MARINELIFEfileURI.add(contentUri.toString());
                MARINELIFErecycler.notifyDataSetChanged();
            }

        }

//        if (requestCode == GALLERY_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri contentUri = data.getData();
//                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//                String imageFileName = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
//                Log.d("tag", "onActivityResult: Gallery Image Uri:  " + imageFileName);
//                selectedImage.setImageURI(contentUri);
//            }
//
//        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data.getClipData() != null) {

                int totalItemsSelected = data.getClipData().getItemCount();

                for (int i = 0; i < totalItemsSelected; i++) {

                    Uri fileUri = data.getClipData().getItemAt(i).getUri();

                    String fileName = getFileName(fileUri);

                    MARINELIFEfileURI.add(fileUri.toString());
                    MARINELIFErecycler.notifyDataSetChanged();
                    Log.e("tagg",MARINELIFEfileURI.get(i));
//                    fileNameList.add(fileName);
//                    fileDoneList.add("uploading");
//                    uploadListAdapter.notifyDataSetChanged();

//                 StorageReference fileToUpload = mStorage.child("Images").child(fileName);
////
//                    final int finalI = i;
//                    fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
////                            fileDoneList.remove(finalI);
////                            fileDoneList.add(finalI, "done");
////
////                            uploadListAdapter.notifyDataSetChanged();
//                            Toast.makeText(ChartActivity.this, "done", Toast.LENGTH_SHORT).show();
////
//                      }
//                    });

                }

                //Toast.makeText(MainActivity.this, "Selected Multiple Files", Toast.LENGTH_SHORT).show();

            } else if (data.getData() != null) {

                Uri im = data.getData();
                MARINELIFEfileURI.add(im.toString());
                MARINELIFErecycler.notifyDataSetChanged();
              //  Toast.makeText(MarineLife.this, "Selected Single File", Toast.LENGTH_SHORT).show();

            }

        }
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);
                    //    txtAddress.setText("Address: "+address);
                    //    txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                    // Toast.makeText(this, ""+selectedLatitude+""+selectedLongitude, Toast.LENGTH_SHORT).show();
                    getFormattedLocationInDegree(selectedLatitude, selectedLongitude);
                    Log.e("taging",selectedLatitude+""+selectedLongitude);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    private void askCameraPermissions() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        } else {
            dispatchTakePictureIntent();
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        MARINELIFEcurrentPhotoPath = image.getAbsolutePath();
        Log.e("current", MARINELIFEcurrentPhotoPath);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
       // Toast.makeText(this, "asd", Toast.LENGTH_SHORT).show();
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
        //    Toast.makeText(this, "asdasdasddasdasd", Toast.LENGTH_SHORT).show();
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.hydrochart.hydrochart.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public  String getFormattedLocationInDegree(double latitude, double longitude) {
        try {

            int latSeconds = (int) Math.round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int) Math.round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            String latDegree = latDegrees >= 0 ? "N" : "S";
            String lonDegrees = longDegrees >= 0 ? "E" : "W";



            MARINELIFELatitudeDegree.getEditText().setText(Math.abs(latDegrees)+"");
            MARINELIFELatitudeMinutes.getEditText().setText(latMinutes+"");
            MARINELIFELatitudeDecimal.getEditText().setText(latSeconds+"");

            MARINELIFELongitudeDegrees.getEditText().setText(Math.abs(longDegrees)+"");
            MARINELIFELongitudeMinutes.getEditText().setText(longMinutes+"");
            MARINELIFELongitudeDecimal.getEditText().setText(longSeconds+"");

            if(latDegree.equals("N"))
            {
              //  Toast.makeText(this, "GOOD", Toast.LENGTH_SHORT).show();
                MARINELIFEspinner_latitude.setSelection(0);
            }
            if(latDegree.equals("S"))
            {
              //  Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                MARINELIFEspinner_latitude.setSelection(1);
            }
            if(lonDegrees.equals("E"))
            {
                MARINELIFEspinner_longtitude.setSelection(0);
            }

            if(lonDegrees.equals("W"))
            {
                MARINELIFEspinner_longtitude.setSelection(1);
            }


            Log.e("tagssss",Math.abs(latDegrees)+""+latMinutes+""+latSeconds+""+Math.abs(longDegrees)+""+longMinutes+""+longSeconds+"");

            return Math.abs(latDegrees) + "°" + latMinutes + "'" + latSeconds
                    + "\"" + latDegree + " " + Math.abs(longDegrees) + "°" + longMinutes
                    + "'" + longSeconds + "\"" + lonDegrees;
        } catch (Exception e) {
            return "" + String.format("%8.5f", latitude) + "  "
                    + String.format("%8.5f", longitude);
        }
    }






    private boolean validateMARINELIFENumberObserved() {
        marinennumberobserved = MARINELIFENumberObserved.getEditText().getText().toString().trim();

        if (marinennumberobserved.isEmpty()) {
            MARINELIFENumberObserved.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFENumberObserved.setError(null);
            return true;
        }
    }


    private boolean validateMARINELIFELength() {
        marinelength = MARINELIFELength.getEditText().getText().toString().trim();

        if (marinelength.isEmpty()) {
            MARINELIFELength.setError("Field can't be empty");
            return false;
        }
        else if(Integer.parseInt(marinelength)<1||Integer.parseInt(marinelength)>9999)
        {
            MARINELIFELength.setError("Length Should Be Between 1-9999");
            return false;
        }

        else  {

            MARINELIFELength.setError(null);
            return true;
        }
    }

    private boolean validateMARINELIFESpecies() {
       marinespecies = MARINELIFESpecies.getEditText().getText().toString().trim();

        if (marinespecies.isEmpty()) {
            MARINELIFESpecies.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFESpecies.setError(null);
            return true;
        }
    }




    private boolean validateMARINELIFELatitudeDegree() {
        marinelatitudedegree =  MARINELIFELatitudeDegree.getEditText().getText().toString().trim();
        if (marinelatitudedegree.isEmpty()) {
            MARINELIFELatitudeDegree.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFELatitudeDegree.setError(null);
            return true;
        }
    }

    private boolean validateMARINELIFELatitudeMinutes() {
        marinelatitudeminutes = MARINELIFELatitudeMinutes.getEditText().getText().toString().trim();
        if (marinelatitudeminutes.isEmpty()) {
            MARINELIFELatitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFELatitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validateMARINELIFELatitudeDecimal() {
        marinelatitudedecimal =  MARINELIFELatitudeDecimal.getEditText().getText().toString().trim();
        if (marinelatitudedecimal.isEmpty()) {
            MARINELIFELatitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFELatitudeDecimal.setError(null);
            return true;
        }
    }

    private boolean validateMARINELIFELongitudeDegree() {
        marinelongitudedegrees = MARINELIFELongitudeDegrees.getEditText().getText().toString().trim();
        if ( marinelongitudedegrees.isEmpty()) {
            MARINELIFELongitudeDegrees.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFELongitudeDegrees.setError(null);
            return true;
        }
    }

    private boolean validateMARINELIFELongitudeMinutes() {
        marinelongtitugeminutes =   MARINELIFELongitudeMinutes.getEditText().getText().toString().trim();
        if (marinelongtitugeminutes.isEmpty()) {
            MARINELIFELongitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFELongitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validateMARINELIFELongitudeDecimal() {
      marinelongitudedecimal =  MARINELIFELongitudeDecimal.getEditText().getText().toString().trim();
        if (marinelongitudedecimal.isEmpty()) {
            MARINELIFELongitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            MARINELIFELongitudeDecimal.setError(null);
            return true;
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String currentDateString = DateFormat.getDateInstance(DateFormat.FULL).format(c.getTime());

        marinelifeDate.setText(currentDateString);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

        marinelifeTime.setText(hourOfDay+":"+ minute);
    }
}
