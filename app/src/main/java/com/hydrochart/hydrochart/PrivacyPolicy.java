package com.hydrochart.hydrochart;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class PrivacyPolicy extends AppCompatActivity {

    TextView link;
    Button privacyBtn;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs" ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        String value = sharedpreferences.getString("DONE", "");
//        Toast.makeText(this, value+"", Toast.LENGTH_SHORT).show();
//        if(value.equals("YES"))
//        {
//            Intent intent = new Intent(PrivacyPolicy.this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//        }



        link=findViewById(R.id.link);
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent1= new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1qYr6c1O4J_DKEUd8BGbvTYhOh4aV1nWSle2kGAGxkP8/edit?usp=sharing"));
                    intent1.setPackage("com.google.android.apps.docs");
                    startActivity(intent1);

                }
                catch (ActivityNotFoundException e)
                {
                    Intent intentj= new Intent(Intent.ACTION_VIEW,Uri.parse("https://docs.google.com/document/d/1qYr6c1O4J_DKEUd8BGbvTYhOh4aV1nWSle2kGAGxkP8/edit?usp=sharing"));
                    startActivity(intentj);
                }
            }
        });

        privacyBtn=findViewById(R.id.acceptBtn);
        privacyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("DONE", "YES");
                editor.commit();
            Intent intent = new Intent(PrivacyPolicy.this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
