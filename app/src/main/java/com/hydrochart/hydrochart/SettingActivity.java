package com.hydrochart.hydrochart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


public class SettingActivity extends AppCompatActivity {
Toolbar toolbar;
TextView privavy;
TextView about;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        toolbar = findViewById(R.id.toolbarsetting);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp);
        about=findViewById(R.id.about);
        privavy=findViewById(R.id.privacypolicy);
        privavy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent1= new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1qYr6c1O4J_DKEUd8BGbvTYhOh4aV1nWSle2kGAGxkP8/edit?usp=sharing"));
                    intent1.setPackage("com.google.android.apps.docs");
                    startActivity(intent1);

                }
                catch (ActivityNotFoundException e)
                {
                    Intent intentj= new Intent(Intent.ACTION_VIEW,Uri.parse("https://docs.google.com/document/d/1qYr6c1O4J_DKEUd8BGbvTYhOh4aV1nWSle2kGAGxkP8/edit?usp=sharing"));
                    startActivity(intentj);
                }
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent1= new Intent(Intent.ACTION_VIEW, Uri.parse("https://docs.google.com/document/d/1rvauYSR-CjuhPvRYmSVIPxhJvqVr070y_8mE_8bigjs/edit?usp=sharing"));
                    intent1.setPackage("com.google.android.apps.docs");
                    startActivity(intent1);

                }
                catch (ActivityNotFoundException e)
                {
                    Intent intentj= new Intent(Intent.ACTION_VIEW,Uri.parse("https://docs.google.com/document/d/1rvauYSR-CjuhPvRYmSVIPxhJvqVr070y_8mE_8bigjs/edit?usp=sharing"));
                    startActivity(intentj);
                }
            }
        });
    }
}
