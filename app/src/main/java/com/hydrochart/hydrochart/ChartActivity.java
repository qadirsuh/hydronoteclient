package com.hydrochart.hydrochart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.core.Tag;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hydrochart.hydrochart.Databases.ChartDraft;
import com.hydrochart.hydrochart.Databases.DetailsDatabase;
import com.hydrochart.hydrochart.Databases.DetailsEntity;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ChartActivity extends AppCompatActivity {
    private static final int ADDRESS_PICKER_REQUEST = 1000;
    Toolbar toolbar;
    Button ChartCamera, ChartGallery, locationPicker,ChartSubmit;
    RecyclerView recyclerView;
    List<String> fileURI;
    List<String> downloadFilePath;
    Recycler recycler;
    String currentPhotoPath;
    DetailsDatabase database;
    private Spinner spinner_latitude,spinner_longtitude;

   public TextInputLayout ChartPrefix, ChartNumber, ChartSuffix, ChartEdition, ChartNMWeek, ChartNMYear, ChartLatitudeDegree, ChartLatitudeMinutes, ChartLatitudeDecimal;
    TextInputLayout ChartLongitudeDegrees, ChartLongitudeMinutes, ChartLongitudeDecimal,ChartSupportingInformation;

    String chartprefix,chartnumber,chartsuffix,chartedition,chartnmweek,chartnmyear,chartlatitudedegree,chartlatitudeminutes,chartlatitudedecimal,chartlongitudedegrees,chartlongtitugeminutes,chartlongitudedecimal;
String latitudepostion,longitudeposition;
String userEmail,userName,userCompany,userOccupation,userHomeCountry,userVesselName,userIMONumber;
String downloadPath;
   String saveCurrentDate,saveCurrentTime,RandomKEY;
String chartsupportinginformation;
    private ProgressDialog Progress;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int GALLERY_REQUEST_CODE = 105;
   // private StorageReference mStorage;
    private StorageReference productImageRef;
    private DatabaseReference productRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chart);
        toolbar = findViewById(R.id.toolbarChart);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        ChartCamera = findViewById(R.id.Camera);
        ChartGallery = findViewById(R.id.Gallary);
        locationPicker = findViewById(R.id.CahrtUseLocationPicker);
        ChartSubmit=findViewById(R.id.submitChartDetails);
        recyclerView = findViewById(R.id.ChartRecycler);
        fileURI = new ArrayList<>();
        recycler = new Recycler(this, fileURI);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(recycler);
     //   mStorage = FirebaseStorage.getInstance().getReference();
        downloadFilePath=new ArrayList<>();
        productImageRef = FirebaseStorage.getInstance().getReference().child("Images");
        productRef = FirebaseDatabase.getInstance().getReference().child("ChartDetails");
        Progress = new ProgressDialog(this);
        MapUtility.apiKey = getResources().getString(R.string.your_api_key);
        database = Room.databaseBuilder(ChartActivity.this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();
        ChartPrefix=findViewById(R.id.ChartPrefix);
        ChartNumber=findViewById(R.id.chartNumber);
        ChartSuffix=findViewById(R.id.ChartSuffix);
        ChartEdition=findViewById(R.id.chartEdition);
        ChartNMWeek=findViewById(R.id.ChartNMWeek);
        ChartNMYear=findViewById(R.id.ChartNMYear);
        ChartLatitudeDegree=findViewById(R.id.ChartLatitudeDegree);
        ChartLatitudeMinutes=findViewById(R.id.ChartLatitudeMinutes);
        ChartLatitudeDecimal=findViewById(R.id.ChartLatitudeDecimal);
        ChartLongitudeDegrees=findViewById(R.id.ChartLongitudeDegrees);
        ChartLongitudeMinutes=findViewById(R.id.ChartLongitudeMinutes);
        ChartLongitudeDecimal=findViewById(R.id.ChartLongitudeDecimal);
        ChartSupportingInformation=findViewById(R.id.ChartSupportingInformation);
        spinner_latitude=findViewById(R.id.spinner_latitude);
        spinner_longtitude=findViewById(R.id.spinner_longitude);

        List<ChartDraft> chartDrafts=new ArrayList<>();

        chartDrafts.addAll(database.getChart().getChartDraft());

     //   Toast.makeText(this, chartDrafts.size()+"", Toast.LENGTH_SHORT).show();

      //  Toast.makeText(this, chartDrafts.get(0).URLIMAGE.get(0)+"sadsad"+chartDrafts.get(0).getEdition()+""+chartDrafts.get(0).getPrefix()+" "+chartDrafts.get(0).getLatitudePosition(), Toast.LENGTH_SHORT).show();

//        Log.e("NEW",chartDrafts.get(0).getURLIMAGE().get(0).toString()+"");


            userName = database.getDetails().getDetails().get(0).getUserName();
            userEmail=   database.getDetails().getDetails().get(0).getEmail();
            userCompany = database.getDetails().getDetails().get(0).getCompany();
            userOccupation = database.getDetails().getDetails().get(0).getOccupation();
            userHomeCountry=  database.getDetails().getDetails().get(0).getHomeCountry();
            userVesselName= database.getDetails().getDetails().get(0).getVesselName();
            userIMONumber= database.getDetails().getDetails().get(0).getImoNumber();


        ChartSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkChartInput();
            }
        });

        ChartCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCameraPermissions();

            }
        });


        ChartGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
            }
        });


        locationPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ChartActivity.this, LocationPickerActivity.class);
                startActivityForResult(i, ADDRESS_PICKER_REQUEST);
            }
        });


        ArrayAdapter<CharSequence> latitude = ArrayAdapter.createFromResource(this,R.array.NS,android.R.layout.simple_spinner_item);

        latitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
      //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        spinner_latitude.setAdapter(latitude);
        spinner_latitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position){
                        case 0:
                           latitudepostion="N";
                           break;
                        case 1:
                            latitudepostion="S";
                            break;

                    }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> longitude = ArrayAdapter.createFromResource(this,R.array.EW,android.R.layout.simple_spinner_item);

        longitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        spinner_longtitude.setAdapter(longitude);
        spinner_longtitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        longitudeposition="E";
                        break;
                    case 1:
                        longitudeposition="W";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Intent intent = getIntent();

        String chartdraft="NO";
        int selector=intent.getIntExtra("ChartDraft",0);

        //Toast.makeText(this, chartdraft+"", Toast.LENGTH_SHORT).show();
        if(selector==1)
        {

            if(chartDrafts.size()>0)
            {

         //   Toast.makeText(this, "DONEEE"+"sdadasd"+database.getChart().getChartDraft().get(0).getLatitudeDecimal(), Toast.LENGTH_SHORT).show();
            ChartPrefix.getEditText().setText(chartDrafts.get(0).getPrefix());
            ChartNumber.getEditText().setText(chartDrafts.get(0).getNumber());
            ChartSuffix.getEditText().setText(chartDrafts.get(0).getSuffix());
            ChartEdition.getEditText().setText(chartDrafts.get(0).getEdition());
            ChartNMWeek.getEditText().setText(chartDrafts.get(0).getNMWeek());
            ChartNMYear.getEditText().setText(chartDrafts.get(0).getNMYear());
            ChartLatitudeDegree.getEditText().setText(chartDrafts.get(0).getLatitudeDegree());
            ChartLatitudeMinutes.getEditText().setText(chartDrafts.get(0).getLatitudeMinutes());
            ChartLatitudeDecimal.getEditText().setText(chartDrafts.get(0).getLatitudeDecimal());
            String latitudeP=chartDrafts.get(0).getLatitudePosition();
            if(latitudeP.equals("N"))
            {
                spinner_latitude.setSelection(0);
            }
            if(latitudeP.equals("S")){
                spinner_latitude.setSelection(1);
            }
            ChartLongitudeDegrees.getEditText().setText(chartDrafts.get(0).getLongitudeDegree());
            ChartLongitudeMinutes.getEditText().setText(chartDrafts.get(0).getLongitudeMinutes());
            ChartLongitudeDecimal.getEditText().setText(chartDrafts.get(0).getLongitudeDecimal());
            String longitudeP=chartDrafts.get(0).getLongitudePosition();
            if(longitudeP.equals("E"))
            {
                spinner_longtitude.setSelection(0);
            }
            if(longitudeP.equals("W")){
                spinner_longtitude.setSelection(1);
            }
            ChartSupportingInformation.getEditText().setText(chartDrafts.get(0).getObservation());
            for(int i=0;i<chartDrafts.get(0).getURLIMAGE().size();i++)
            {

                if(!chartDrafts.get(0).getURLIMAGE().get(i).contains("content:"))
                {
                    fileURI.add(chartDrafts.get(0).getURLIMAGE().get(i));
                }
                Log.e("img",chartDrafts.get(0).getURLIMAGE().get(i));

            }
            recycler.notifyDataSetChanged();

        }

        }

    }

    private void checkChartInput() {

        if(!validateChartPrefix()|!validateChartNumber()|!validateChartEdition()|!validateChartNMWeek()|!validateChartNMYear()|!validateChartLatitudeDegree()|!validateChartLatitudeMinutes()|!validateChartLatitudeDecimal()|!validateChartLongitudeDegree()|!validateChartLongitudeMinutes()|!validateChartLongitudeDecimal())
        {
            return;
        }

        chartsupportinginformation = ChartSupportingInformation.getEditText().getText().toString().trim();



     //   Toast.makeText(this, "DONE", Toast.LENGTH_SHORT).show();
      Progress.setTitle("Uploading Information..");
       Progress.setMessage("PLEASE WAIT WE ARE UPLOADING INFORMATION..");
       Progress.setCanceledOnTouchOutside(false);
        Progress.show();
        SaveData();
    }

    private void SaveData() {
        downloadFilePath.clear();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a"); // it will give us the currentTime a means am and pm
        saveCurrentTime = currentTime.format(calendar.getTime());
        RandomKEY = saveCurrentDate + saveCurrentTime;
        if (fileURI.size() > 0) {
            for (int i = 0; i < fileURI.size(); i++) {
                final StorageReference filePath;
                filePath = productImageRef.child(Uri.parse(fileURI.get(i)).getLastPathSegment() + RandomKEY + ".jpg"); //last path segment is used to get the imageuri last / part which is basically image name in the uri
                final UploadTask uploadTask = filePath.putFile(Uri.parse(fileURI.get(i)));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String messege = e.toString();
                        Progress.dismiss();
                        Toast.makeText(ChartActivity.this, "Error: " + messege + "..", Toast.LENGTH_SHORT).show();

                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { // tasksnapshot show about the state of the running upload task
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                 //       Toast.makeText(ChartActivity.this, "Image Stored Successfully..", Toast.LENGTH_SHORT).show();

                        Task<Uri> taskUrl = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {

                                    throw task.getException();
                                }
                                downloadPath = filePath.getDownloadUrl().toString();

                                return filePath.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {

                                    downloadPath = task.getResult().toString();
                                    Log.e("image", downloadPath);
                                    //Toast.makeText(ChartActivity.this, downloadPath + "", Toast.LENGTH_SHORT).show();
                                    downloadFilePath.add(downloadPath);
                                   // Toast.makeText(ChartActivity.this, "Product Image URL Got Successfully..", Toast.LENGTH_SHORT).show();
                                    // SaveProductInfoToDatabase();
                                Log.e("TAGINGINGIG",downloadFilePath.size()+"  "+ fileURI.size());
                                    if (downloadFilePath.size() == fileURI.size()) {
                                        SaveProductInfoToChart();
                                    }
                                }
                            }
                        });
                    }
                });

            }
            Log.e("calling", "work");

            //    SaveProductInfoToChart();
        }

        else
        {

            HashMap<String ,Object> hashMap= new HashMap<>();
            // hashMap.put("image",downloadFilePath.get(0));
            hashMap.put("UserName",userName);
            hashMap.put("UserEmail",userEmail);
            hashMap.put("UserCompany",userCompany);
            hashMap.put("UserOccupation",userOccupation);
            hashMap.put("UserHomeCountry",userHomeCountry);
            hashMap.put("UserVesselName",userVesselName);
            hashMap.put("UserIMONumber",userIMONumber);
            hashMap.put("ChartPrefix",chartprefix);
            hashMap.put("ChartNumber",chartnumber);
            hashMap.put("ChartSuffix",chartsuffix);
            hashMap.put("ChartEdition",chartedition);
            hashMap.put("ChartNMWeek",chartnmweek);
            hashMap.put("ChartNMYear",chartnmyear);
            hashMap.put("ChartLatitudeDegree",chartlatitudedegree);
            hashMap.put("ChartLatitudeMinutes",chartlatitudeminutes);
            hashMap.put("ChartLatitudeDecimals",chartlatitudedecimal);
            hashMap.put("ChartLatitudePosition",latitudepostion);
            hashMap.put("ChartLongitudeDegree",chartlongitudedegrees);
            hashMap.put("ChartLongitudeMinutes",chartlongtitugeminutes);
            hashMap.put("ChartLongitudeDecimals",chartlongitudedecimal);
            hashMap.put("ChartLongitudePosition",longitudeposition);
            hashMap.put("ChartSupportingInformation",chartsupportinginformation);
            hashMap.put("date",saveCurrentDate);
            hashMap.put("time",saveCurrentTime);






            productRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {

                        Toast.makeText(ChartActivity.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                        Progress.dismiss();
                    }
                    else {
                        //   loginProgress.dismiss();
                        String messege =task.getException().toString();
                        Progress.dismiss();
                        Toast.makeText(ChartActivity.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

    }


    private void SaveProductInfoToChart() {
        HashMap<String ,Object> hashMap= new HashMap<>();
       // hashMap.put("image",downloadFilePath.get(0));
        for(int i=0;i<downloadFilePath.size();i++)
        {
            hashMap.put("image"+i,downloadFilePath.get(i));
        }
        hashMap.put("UserName",userName);
        hashMap.put("UserEmail",userEmail);
        hashMap.put("UserCompany",userCompany);
        hashMap.put("UserOccupation",userOccupation);
        hashMap.put("UserHomeCountry",userHomeCountry);
        hashMap.put("UserVesselName",userVesselName);
        hashMap.put("UserIMONumber",userIMONumber);
        hashMap.put("ChartPrefix",chartprefix);
        hashMap.put("ChartNumber",chartnumber);
        hashMap.put("ChartSuffix",chartsuffix);
        hashMap.put("ChartEdition",chartedition);
        hashMap.put("ChartNMWeek",chartnmweek);
        hashMap.put("ChartNMYear",chartnmyear);
        hashMap.put("ChartLatitudeDegree",chartlatitudedegree);
        hashMap.put("ChartLatitudeMinutes",chartlatitudeminutes);
        hashMap.put("ChartLatitudeDecimals",chartlatitudedecimal);
        hashMap.put("ChartLatitudePosition",latitudepostion);
        hashMap.put("ChartLongitudeDegree",chartlongitudedegrees);
        hashMap.put("ChartLongitudeMinutes",chartlongtitugeminutes);
        hashMap.put("ChartLongitudeDecimals",chartlongitudedecimal);
        hashMap.put("ChartLongitudePosition",longitudeposition);
        hashMap.put("ChartSupportingInformation",chartsupportinginformation);
        hashMap.put("date",saveCurrentDate);
        hashMap.put("time",saveCurrentTime);






        productRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Progress.dismiss();
                    Toast.makeText(ChartActivity.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                 //   loginProgress.dismiss();
                    Progress.dismiss();
                    String messege =task.getException().toString();
                    Toast.makeText(ChartActivity.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERM_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        database = Room.databaseBuilder(ChartActivity.this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

                        List<ChartDraft> chartDrafts = new ArrayList<>();

                        chartDrafts.addAll(database.getChart().getChartDraft());


//                        Toast.makeText(ChartActivity.this, chartDrafts.size()+""+database.getChart().getChartDraft().get(0).getEdition()+"", Toast.LENGTH_SHORT).show();

                        if(chartDrafts.size()>0) {

                          //  Log.e("Tag",draftList.get(0).getPrefix()+""+draftList.get(0).URLIMAGE.get(0));
                         //   Toast.makeText(ChartActivity.this, "UPDATE", Toast.LENGTH_SHORT).show();

                            ChartDraft chartDraft = new ChartDraft(0,ChartPrefix.getEditText().getText().toString(),ChartNumber.getEditText().getText().toString(),ChartSuffix.getEditText().getText().toString(),ChartEdition.getEditText().getText().toString(),ChartNMWeek.getEditText().getText().toString(),ChartNMYear.getEditText().getText().toString(),ChartLatitudeDegree.getEditText().getText().toString(),ChartLatitudeMinutes.getEditText().getText().toString(),ChartLatitudeDecimal.getEditText().getText().toString(),latitudepostion,ChartLongitudeDegrees.getEditText().getText().toString(),ChartLongitudeMinutes.getEditText().getText().toString(),ChartLongitudeDecimal.getEditText().getText().toString(),longitudeposition,ChartSupportingInformation.getEditText().getText().toString(),fileURI);
                            database.getChart().updateDetails(chartDraft);

                        //    Log.e("Tag",chartDrafts.get(0).URLIMAGE.get(0)+"");
                        }
                        else
                        {

                        //    Toast.makeText(ChartActivity.this, "Insert", Toast.LENGTH_SHORT).show();

                            ChartDraft chartDraft = new ChartDraft(0,ChartPrefix.getEditText().getText().toString(),ChartNumber.getEditText().getText().toString(),ChartSuffix.getEditText().getText().toString(),ChartEdition.getEditText().getText().toString(),ChartNMWeek.getEditText().getText().toString(),ChartNMYear.getEditText().getText().toString(),ChartLatitudeDegree.getEditText().getText().toString(),ChartLatitudeMinutes.getEditText().getText().toString(),ChartLatitudeDecimal.getEditText().getText().toString(),latitudepostion,ChartLongitudeDegrees.getEditText().getText().toString(),ChartLongitudeMinutes.getEditText().getText().toString(),ChartLongitudeDecimal.getEditText().getText().toString(),longitudeposition,ChartSupportingInformation.getEditText().getText().toString(),fileURI);
                            database.getChart().addChartDraft(chartDraft);

                        //    Log.e("Tag",chartDrafts.get(0).URLIMAGE.get(0)+"");
                        }



                        finish();
                        //Yes button clicked
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked


                       finish();
                        break;
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(ChartActivity.this);
        builder.setMessage("Would You Like To Save Draft?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(currentPhotoPath);
                //selectedImage.setImageURI(Uri.fromFile(f));
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                //  f.getAbsolutePath();
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                Log.e("api", contentUri.toString());

                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                fileURI.add(contentUri.toString());
                recycler.notifyDataSetChanged();
            }

        }

//        if (requestCode == GALLERY_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri contentUri = data.getData();
//                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//                String imageFileName = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
//                Log.d("tag", "onActivityResult: Gallery Image Uri:  " + imageFileName);
//                selectedImage.setImageURI(contentUri);
//            }
//
//        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data.getClipData() != null) {

                int totalItemsSelected = data.getClipData().getItemCount();

                for (int i = 0; i < totalItemsSelected; i++) {

                    Uri fileUri = data.getClipData().getItemAt(i).getUri();

                    String fileName = getFileName(fileUri);

                    fileURI.add(fileUri.toString());
                    recycler.notifyDataSetChanged();
                    Log.e("tagg", fileURI.get(i));
//                    fileNameList.add(fileName);
//                    fileDoneList.add("uploading");
//                    uploadListAdapter.notifyDataSetChanged();

//                 StorageReference fileToUpload = mStorage.child("Images").child(fileName);
////
//                    final int finalI = i;
//                    fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
////                            fileDoneList.remove(finalI);
////                            fileDoneList.add(finalI, "done");
////
////                            uploadListAdapter.notifyDataSetChanged();
//                            Toast.makeText(ChartActivity.this, "done", Toast.LENGTH_SHORT).show();
////
//                      }
//                    });

                }

                //Toast.makeText(MainActivity.this, "Selected Multiple Files", Toast.LENGTH_SHORT).show();

            } else if (data.getData() != null) {

                Uri im = data.getData();
                fileURI.add(im.toString());
                recycler.notifyDataSetChanged();
            //    Toast.makeText(ChartActivity.this, "Selected Single File", Toast.LENGTH_SHORT).show();

            }

        }
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);
                    //    txtAddress.setText("Address: "+address);
                    //    txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                    // Toast.makeText(this, ""+selectedLatitude+""+selectedLongitude, Toast.LENGTH_SHORT).show();
                 getFormattedLocationInDegree(selectedLatitude, selectedLongitude);
                    Log.e("taging",selectedLatitude+""+selectedLongitude);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    private void askCameraPermissions() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        } else {
            dispatchTakePictureIntent();
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        Log.e("current", currentPhotoPath);
        return image;
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
    //    Toast.makeText(this, "asd", Toast.LENGTH_SHORT).show();
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
         //   Toast.makeText(this, "asdasdasddasdasd", Toast.LENGTH_SHORT).show();
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.hydrochart.hydrochart.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public  String getFormattedLocationInDegree(double latitude, double longitude) {
        try {

            int latSeconds = (int) Math.round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int) Math.round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            String latDegree = latDegrees >= 0 ? "N" : "S";
            String lonDegrees = longDegrees >= 0 ? "E" : "W";



            ChartLatitudeDegree.getEditText().setText(Math.abs(latDegrees)+"");
            ChartLatitudeMinutes.getEditText().setText(latMinutes+"");
            ChartLatitudeDecimal.getEditText().setText(latSeconds+"");

            ChartLongitudeDegrees.getEditText().setText(Math.abs(longDegrees)+"");
            ChartLongitudeMinutes.getEditText().setText(longMinutes+"");
            ChartLongitudeDecimal.getEditText().setText(longSeconds+"");

            if(latDegree.equals("N"))
            {
             //   Toast.makeText(this, "GOOD", Toast.LENGTH_SHORT).show();
                spinner_latitude.setSelection(0);
            }
            if(latDegree.equals("S"))
            {
          //      Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                spinner_latitude.setSelection(1);
            }
            if(lonDegrees.equals("E"))
            {
                spinner_longtitude.setSelection(0);
            }

            if(lonDegrees.equals("W"))
            {
                spinner_longtitude.setSelection(1);
            }


            Log.e("tagssss",Math.abs(latDegrees)+""+latMinutes+""+latSeconds+""+Math.abs(longDegrees)+""+longMinutes+""+longSeconds+"");

            return Math.abs(latDegrees) + "°" + latMinutes + "'" + latSeconds
                    + "\"" + latDegree + " " + Math.abs(longDegrees) + "°" + longMinutes
                    + "'" + longSeconds + "\"" + lonDegrees;
        } catch (Exception e) {
            return "" + String.format("%8.5f", latitude) + "  "
                    + String.format("%8.5f", longitude);
        }
    }

    private boolean validateChartPrefix() {
        chartprefix = ChartPrefix.getEditText().getText().toString().trim();

        if (chartprefix.isEmpty()) {
            ChartPrefix.setError("Field can't be empty");
            return false;
        }


        else  {

            ChartPrefix.setError(null);
            return true;
        }
    }

    private boolean validateChartNumber() {
        chartnumber = ChartNumber.getEditText().getText().toString().trim();

        if (chartnumber.isEmpty()) {
            ChartNumber.setError("Field can't be empty");
            return false;
        }


        else  {

            ChartNumber.setError(null);
            return true;
        }
    }
    private boolean validateChartSuffix() {
        chartsuffix = ChartSuffix.getEditText().getText().toString().trim();

        if (chartsuffix.isEmpty()) {
            ChartSuffix.setError("Field can't be empty");
            return false;
        }


        else  {

            ChartSuffix.setError(null);
            return true;
        }
    }

    private boolean validateChartEdition() {
        chartedition = ChartEdition.getEditText().getText().toString().trim();

        if (chartedition.isEmpty()) {
            ChartEdition.setError("Field can't be empty");
            return false;
        }
        else if(Integer.parseInt(chartedition)<=0||Integer.parseInt(chartedition)>9999)
        {
            ChartEdition.setError("Edition Should Be Between 1-9999");
            return false;
        }

        else  {

            ChartEdition.setError(null);
            return true;
        }
    }

    private boolean validateChartNMWeek() {
        chartnmweek = ChartNMWeek.getEditText().getText().toString().trim();

        if (chartnmweek.isEmpty()) {
            ChartNMWeek.setError("Field can't be empty");
            return false;
        }
        else if(Integer.parseInt(chartnmweek)<=0||Integer.parseInt(chartnmweek)>53)
        {
            ChartNMWeek.setError("Chart NM Week Should Be Between 1-53");
            return false;
        }

        else  {

            ChartNMWeek.setError(null);
            return true;
        }
    }

    private boolean validateChartNMYear() {
        chartnmyear = ChartNMYear.getEditText().getText().toString().trim();
       // Toast.makeText(this, "chartdw"+chartnmyear, Toast.LENGTH_SHORT).show();
       int date= Calendar.getInstance().get(Calendar.YEAR);
        if (chartnmyear.isEmpty()) {
            ChartNMYear.setError("Field can't be empty");

            return false;
        }
        else if(Integer.parseInt(chartnmyear)<1700||Integer.parseInt(chartnmyear)>date)
        {
            ChartNMYear.setError("Chart NM Year Should Be Between 1700-"+date);
            return false;
        }
        else  {

            ChartNMYear.setError(null);
            return true;
        }
    }

    private boolean validateChartLatitudeDegree() {
        chartlatitudedegree = ChartLatitudeDegree.getEditText().getText().toString().trim();
        if (chartlatitudedegree.isEmpty()) {
            ChartLatitudeDegree.setError("Field can't be empty");
            return false;
        }

        else  {

            ChartLatitudeDegree.setError(null);
            return true;
        }
    }

    private boolean validateChartLatitudeMinutes() {
        chartlatitudeminutes = ChartLatitudeMinutes.getEditText().getText().toString().trim();
        if (chartlatitudeminutes.isEmpty()) {
            ChartLatitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            ChartLatitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validateChartLatitudeDecimal() {
        chartlatitudedecimal = ChartLatitudeDecimal.getEditText().getText().toString().trim();
        if (chartlatitudedecimal.isEmpty()) {
            ChartLatitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            ChartLatitudeDecimal.setError(null);
            return true;
        }
    }

    private boolean validateChartLongitudeDegree() {
        chartlongitudedegrees = ChartLongitudeDegrees.getEditText().getText().toString().trim();
        if (chartlongitudedegrees.isEmpty()) {
            ChartLongitudeDegrees.setError("Field can't be empty");
            return false;
        }

        else  {

            ChartLongitudeDegrees.setError(null);
            return true;
        }
    }

    private boolean validateChartLongitudeMinutes() {
        chartlongtitugeminutes = ChartLongitudeMinutes.getEditText().getText().toString().trim();
        if (chartlongtitugeminutes.isEmpty()) {
            ChartLongitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            ChartLongitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validateChartLongitudeDecimal() {
        chartlongitudedecimal = ChartLongitudeDecimal.getEditText().getText().toString().trim();
        if (chartlongitudedecimal.isEmpty()) {
            ChartLongitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            ChartLongitudeDecimal.setError(null);
            return true;
        }
    }
}

