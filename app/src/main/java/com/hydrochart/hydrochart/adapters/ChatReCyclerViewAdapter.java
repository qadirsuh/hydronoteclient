package com.hydrochart.hydrochart.adapters;

import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;


import com.hydrochart.hydrochart.R;
import com.hydrochart.hydrochart.models.ChatWithAdmin;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ChatReCyclerViewAdapter extends RecyclerView.Adapter<ChatReCyclerViewAdapter.ViewHolder> {

    private ArrayList<ChatWithAdmin> listData;

    private int viewTypeLeft = 0;
    private int viewTypeRight = 1;
    private String userIdToChatWith = "";

    // RecyclerView recyclerView;
    public ChatReCyclerViewAdapter(ArrayList<ChatWithAdmin> listData, String userIdToChatWith) {
        this.listData = listData;
        this.userIdToChatWith = userIdToChatWith;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem;
        if (viewType == viewTypeLeft) {
            listItem = layoutInflater.inflate(R.layout.chat_item_left, parent, false);
        } else {
            listItem = layoutInflater.inflate(R.layout.chat_item_right, parent, false);
        }
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ChatWithAdmin myListData = listData.get(position);


        try {

            if (myListData.getAttachment() != null) {

                holder.textView.setVisibility(View.GONE);
                holder.imageView.setVisibility(View.VISIBLE);

                final String url = myListData.getDownloadUrl();

                Picasso.get()
                        .load(url)
                        .placeholder(R.drawable.img_placeholder)
                        .error(R.drawable.img_placeholder)
                        .into(holder.imageView);

                holder.imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        holder.imageView.getContext().startActivity(i);
                    }
                });

            } else {
                holder.imageView.setVisibility(View.GONE);
                holder.imageView.setOnClickListener(null);
                holder.textView.setVisibility(View.VISIBLE);
                holder.textView.setText(myListData.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return listData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.show_message);
            this.imageView = (ImageView) itemView.findViewById(R.id.show_image);
        }
    }

    @Override
    public int getItemViewType(int position) {
        // return super.getItemViewType(position);

        if (listData.get(position).getCreatedByUser().equalsIgnoreCase(userIdToChatWith)) {
            return viewTypeRight;
        } else {
            return viewTypeLeft;
        }
    }
}

