package com.hydrochart.hydrochart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hydrochart.hydrochart.Databases.ChartDraft;
import com.hydrochart.hydrochart.Databases.DetailsDatabase;
import com.hydrochart.hydrochart.Databases.ENCDraft;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ENCActivity extends AppCompatActivity {
    private static final int ADDRESS_PICKER_REQUEST = 1000;
    Toolbar toolbar;
    Button ENCCamera, ENCGallery, ENClocationPicker,ENCSubmit;
    RecyclerView recyclerView;
    List<String> ENCfileURI;
    List<String> ENCdownloadFilePath;
   private Recycler ENCrecycler;
    String ENCcurrentPhotoPath;
    DetailsDatabase ENCdatabase;
    private Spinner ENCspinner_latitude,ENCspinner_longtitude;


    public TextInputLayout  ENCAffected, ENCNMWeek, ENCNMYear, ENCLatitudeDegree, ENCLatitudeMinutes, ENCLatitudeDecimal;
    TextInputLayout ENCLongitudeDegrees, ENCLongitudeMinutes, ENCLongitudeDecimal,ENCSupportingInformation;

    String encaffected,encnmweek,encnmyear,enclatitudedegree,enclatitudeminutes,enclatitudedecimal,enclongitudedegrees,enclongtitugeminutes,enclongitudedecimal;
    String enclatitudepostion,enclongitudeposition;
    String userEmail,userName,userCompany,userOccupation,userHomeCountry,userVesselName,userIMONumber;
    String downloadPath;
    String saveCurrentDate,saveCurrentTime,RandomKEY;
    String encsupportinginformation;
    private ProgressDialog Progress;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int GALLERY_REQUEST_CODE = 105;
    // private StorageReference mStorage;
    private StorageReference encImageRef;
    private DatabaseReference encdataRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enc);

        toolbar = findViewById(R.id.toolbarenc);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ENCCamera = findViewById(R.id.encCamera);
        ENCGallery = findViewById(R.id.encGallary);
        ENClocationPicker = findViewById(R.id.encUseLocationPicker);
        ENCSubmit=findViewById(R.id.submitencDetails);
        recyclerView = findViewById(R.id.encRecycler);
        ENCfileURI = new ArrayList<>();
        ENCrecycler = new Recycler(this, ENCfileURI);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(ENCrecycler);
        //   mStorage = FirebaseStorage.getInstance().getReference();
        ENCdownloadFilePath=new ArrayList<>();
        encImageRef = FirebaseStorage.getInstance().getReference().child("Images");
        encdataRef = FirebaseDatabase.getInstance().getReference().child("ENCDetails");
        Progress = new ProgressDialog(this);
        MapUtility.apiKey = getResources().getString(R.string.your_api_key);
        ENCdatabase = Room.databaseBuilder(this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

        ENCAffected=findViewById(R.id.encAffected);
        ENCNMWeek=findViewById(R.id.encNMWeek);
        ENCNMYear=findViewById(R.id.encNMYear);
        ENCLatitudeDegree=findViewById(R.id.encLatitudeDegree);
        ENCLatitudeMinutes=findViewById(R.id.encLatitudeMinutes);
        ENCLatitudeDecimal=findViewById(R.id.encLatitudeDecimal);
        ENCLongitudeDegrees=findViewById(R.id.encLongitudeDegrees);
        ENCLongitudeMinutes=findViewById(R.id.encLongitudeMinutes);
        ENCLongitudeDecimal=findViewById(R.id.encLongitudeDecimal);
        ENCSupportingInformation=findViewById(R.id.encSupportingInformation);
        ENCspinner_latitude=findViewById(R.id.encspinner_latitude);
        ENCspinner_longtitude=findViewById(R.id.encspinner_longitude);


        List<ENCDraft> encDrafts=new ArrayList<>();

        encDrafts.addAll(ENCdatabase.getENCDraft().getENCDraft());




        userName = ENCdatabase.getDetails().getDetails().get(0).getUserName();
        userEmail=   ENCdatabase.getDetails().getDetails().get(0).getEmail();
        userCompany = ENCdatabase.getDetails().getDetails().get(0).getCompany();
        userOccupation = ENCdatabase.getDetails().getDetails().get(0).getOccupation();
        userHomeCountry=  ENCdatabase.getDetails().getDetails().get(0).getHomeCountry();
        userVesselName= ENCdatabase.getDetails().getDetails().get(0).getVesselName();
        userIMONumber= ENCdatabase.getDetails().getDetails().get(0).getImoNumber();

        ENCSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkChartInput();
            }
        });

        ENCCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCameraPermissions();

            }
        });


        ENCGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
            }
        });


        ENClocationPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ENCActivity.this, LocationPickerActivity.class);
                startActivityForResult(i, ADDRESS_PICKER_REQUEST);
            }
        });


        ArrayAdapter<CharSequence> latitude = ArrayAdapter.createFromResource(this,R.array.NS,android.R.layout.simple_spinner_item);

        latitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        ENCspinner_latitude.setAdapter(latitude);
        ENCspinner_latitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        enclatitudepostion="N";
                        break;
                    case 1:
                        enclatitudepostion="S";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> longitude = ArrayAdapter.createFromResource(this,R.array.EW,android.R.layout.simple_spinner_item);

        longitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        ENCspinner_longtitude.setAdapter(longitude);
        ENCspinner_longtitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        enclongitudeposition="E";
                        break;
                    case 1:
                        enclongitudeposition="W";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        Intent intent = getIntent();

        String chartdraft="NO";
        int selector=intent.getIntExtra("ENCDraft",0);

        //Toast.makeText(this, chartdraft+"", Toast.LENGTH_SHORT).show();
        if(selector==1)
        {

            if(encDrafts.size()>0)
            {


            //Toast.makeText(this, "DONEEE"+"sdadasd"+database.getChart().getChartDraft().get(0).getLatitudeDecimal(), Toast.LENGTH_SHORT).show();
            ENCAffected.getEditText().setText(encDrafts.get(0).getENCAffected());
            ENCNMWeek.getEditText().setText(encDrafts.get(0).getNMWeek());
            ENCNMYear.getEditText().setText(encDrafts.get(0).getNMYear());
            ENCLatitudeDegree.getEditText().setText(encDrafts.get(0).getLatitudeDegree());
            ENCLatitudeMinutes.getEditText().setText(encDrafts.get(0).getLatitudeMinutes());
            ENCLatitudeDecimal.getEditText().setText(encDrafts.get(0).getLatitudeDecimal());
            String latitudeP=encDrafts.get(0).getLatitudePosition();

         //   Toast.makeText(this, latitudeP+"", Toast.LENGTH_SHORT).show();
            if(latitudeP.equals("N"))
            {
                //  Toast.makeText(this, "N", Toast.LENGTH_SHORT).show();
                ENCspinner_latitude.setSelection(0);
            }
            if(latitudeP.equals("S")){
                //  Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                ENCspinner_latitude.setSelection(1);
            }
            ENCLongitudeDegrees.getEditText().setText(encDrafts.get(0).getLongitudeDegree());
            ENCLongitudeMinutes.getEditText().setText(encDrafts.get(0).getLongitudeMinutes());
            ENCLongitudeDecimal.getEditText().setText(encDrafts.get(0).getLongitudeDecimal());
            String longitudeP=encDrafts.get(0).getLongitudePosition();
           // Toast.makeText(this, longitudeP+"", Toast.LENGTH_SHORT).show();

            if(longitudeP.equals("E"))
            {
                //     Toast.makeText(this, "E", Toast.LENGTH_SHORT).show();
                ENCspinner_longtitude.setSelection(0);
            }
            if(longitudeP.equals("W")){
                //    Toast.makeText(this, "W", Toast.LENGTH_SHORT).show();
                ENCspinner_longtitude.setSelection(1);
            }


            ENCSupportingInformation.getEditText().setText(encDrafts.get(0).getObservation());
            for(int i=0;i<encDrafts.get(0).getURLIMAGE().size();i++)
            {

                if(!encDrafts.get(0).getURLIMAGE().get(i).contains("content:"))
                {
                    ENCfileURI.add(encDrafts.get(0).getURLIMAGE().get(i));
                }
                Log.e("img",encDrafts.get(0).getURLIMAGE().get(i));

            }
            ENCrecycler.notifyDataSetChanged();

        }
        }

    }


    private void checkChartInput() {

        if(!validateENCAffected()|!validateENCNMWeek()|!validateENCNMYear()|!validateENCLatitudeDegree()|!validateENCLatitudeMinutes()|!validateENCLatitudeDecimal()|!validateENCLongitudeDegree()|!validateENCLongitudeMinutes()|!validateENCLongitudeDecimal())
        {
            return;
        }

        encsupportinginformation = ENCSupportingInformation.getEditText().getText().toString().trim();



     //   Toast.makeText(this, "DONE", Toast.LENGTH_SHORT).show();
        Progress.setTitle("Uploading Information..");
        Progress.setMessage("PLEASE WAIT WE ARE UPLOADING INFORMATION..");
        Progress.setCanceledOnTouchOutside(false);
        Progress.show();
        SaveData();
    }

    private void SaveData() {
        ENCdownloadFilePath.clear();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a"); // it will give us the currentTime a means am and pm
        saveCurrentTime = currentTime.format(calendar.getTime());
        RandomKEY = saveCurrentDate + saveCurrentTime;
        if (ENCfileURI.size() > 0) {
            for (int i = 0; i < ENCfileURI.size(); i++) {
                final StorageReference filePath;
                filePath = encImageRef.child(Uri.parse(ENCfileURI.get(i)).getLastPathSegment() + RandomKEY + ".jpg"); //last path segment is used to get the imageuri last / part which is basically image name in the uri
                final UploadTask uploadTask = filePath.putFile(Uri.parse(ENCfileURI.get(i)));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String messege = e.toString();
                        Progress.dismiss();
                        Toast.makeText(ENCActivity.this, "Error: " + messege + "..", Toast.LENGTH_SHORT).show();

                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { // tasksnapshot show about the state of the running upload task
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //       Toast.makeText(ChartActivity.this, "Image Stored Successfully..", Toast.LENGTH_SHORT).show();

                        Task<Uri> taskUrl = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {

                                    throw task.getException();
                                }
                                downloadPath = filePath.getDownloadUrl().toString();

                                return filePath.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {

                                    downloadPath = task.getResult().toString();
                                    Log.e("image", downloadPath);
                                    //Toast.makeText(ChartActivity.this, downloadPath + "", Toast.LENGTH_SHORT).show();
                                    ENCdownloadFilePath.add(downloadPath);
                                    // Toast.makeText(ChartActivity.this, "Product Image URL Got Successfully..", Toast.LENGTH_SHORT).show();
                                    // SaveProductInfoToDatabase();
                                    Log.e("TAGINGINGIG",ENCdownloadFilePath.size()+"  "+ ENCfileURI.size());
                                    if (ENCdownloadFilePath.size() == ENCfileURI.size()) {
                                        SaveProductInfoToENC();
                                    }
                                }
                            }
                        });
                    }
                });

            }
            Log.e("calling", "work");

            //    SaveProductInfoToChart();
        }

        else
        {

            HashMap<String ,Object> hashMap= new HashMap<>();
            // hashMap.put("image",downloadFilePath.get(0));
            hashMap.put("UserName",userName);
            hashMap.put("UserEmail",userEmail);
            hashMap.put("UserCompany",userCompany);
            hashMap.put("UserOccupation",userOccupation);
            hashMap.put("UserHomeCountry",userHomeCountry);
            hashMap.put("UserVesselName",userVesselName);
            hashMap.put("UserIMONumber",userIMONumber);
            hashMap.put("ENCAffected",encaffected);
            hashMap.put("ENCNMWeek",encnmweek);
            hashMap.put("ENCNMYear",encnmyear);
            hashMap.put("ENCLatitudeDegree",enclatitudedegree);
            hashMap.put("ENCLatitudeMinutes",enclatitudeminutes);
            hashMap.put("ENCLatitudeDecimals",enclatitudedecimal);
            hashMap.put("ENCLatitudePosition",enclatitudepostion);
            hashMap.put("ENCLongitudeDegree",enclongitudedegrees);
            hashMap.put("ENCLongitudeMinutes",enclongtitugeminutes);
            hashMap.put("ENCLongitudeDecimals",enclongitudedecimal);
            hashMap.put("ENCLongitudePosition",enclongitudeposition);
            hashMap.put("ENCSupportingInformation",encsupportinginformation);
            hashMap.put("date",saveCurrentDate);
            hashMap.put("time",saveCurrentTime);






            encdataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {

                        Toast.makeText(ENCActivity.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                        Progress.dismiss();
                    }
                    else {
                        //   loginProgress.dismiss();
                        String messege =task.getException().toString();
                        Progress.dismiss();
                        Toast.makeText(ENCActivity.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


    }

    private void SaveProductInfoToENC() {
        HashMap<String ,Object> hashMap= new HashMap<>();
        // hashMap.put("image",downloadFilePath.get(0));
        for(int i=0;i<ENCdownloadFilePath.size();i++)
        {
            hashMap.put("image"+i,ENCdownloadFilePath.get(i));
        }
        hashMap.put("UserName",userName);
        hashMap.put("UserEmail",userEmail);
        hashMap.put("UserCompany",userCompany);
        hashMap.put("UserOccupation",userOccupation);
        hashMap.put("UserHomeCountry",userHomeCountry);
        hashMap.put("UserVesselName",userVesselName);
        hashMap.put("UserIMONumber",userIMONumber);
        hashMap.put("ENCAffected",encaffected);
        hashMap.put("ENCNMWeek",encnmweek);
        hashMap.put("ENCNMYear",encnmyear);
        hashMap.put("ENCLatitudeDegree",enclatitudedegree);
        hashMap.put("ENCLatitudeMinutes",enclatitudeminutes);
        hashMap.put("ENCLatitudeDecimals",enclatitudedecimal);
        hashMap.put("ENCLatitudePosition",enclatitudepostion);
        hashMap.put("ENCLongitudeDegree",enclongitudedegrees);
        hashMap.put("ENCLongitudeMinutes",enclongtitugeminutes);
        hashMap.put("ENCLongitudeDecimals",enclongitudedecimal);
        hashMap.put("ENCLongitudePosition",enclongitudeposition);
        hashMap.put("ENCSupportingInformation",encsupportinginformation);
        hashMap.put("date",saveCurrentDate);
        hashMap.put("time",saveCurrentTime);





        encdataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Progress.dismiss();
                    Toast.makeText(ENCActivity.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    //   loginProgress.dismiss();
                    Progress.dismiss();
                    String messege =task.getException().toString();
                    Toast.makeText(ENCActivity.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERM_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        ENCdatabase = Room.databaseBuilder(ENCActivity.this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

                        List<ENCDraft> encDrafts = new ArrayList<>();

                        encDrafts.addAll(ENCdatabase.getENCDraft().getENCDraft());

                          //Log.e("Tag",draftList.get(0).URLIMAGE.get(0));
                        if(encDrafts.size()>0) {

                          //  Toast.makeText(ENCActivity.this, "UPDATE", Toast.LENGTH_SHORT).show();
                            ENCDraft encDraft = new ENCDraft(0,ENCAffected.getEditText().getText().toString(),ENCNMWeek.getEditText().getText().toString(),ENCNMYear.getEditText().getText().toString(),ENCLatitudeDegree.getEditText().getText().toString(),ENCLatitudeMinutes.getEditText().getText().toString(),ENCLatitudeDecimal.getEditText().getText().toString(),enclatitudepostion,ENCLongitudeDegrees.getEditText().getText().toString(),ENCLongitudeMinutes.getEditText().getText().toString(),ENCLongitudeDecimal.getEditText().getText().toString(),enclongitudeposition,ENCSupportingInformation.getEditText().getText().toString(),ENCfileURI);

                            ENCdatabase.getENCDraft().updateENC(encDraft);
                        }
                        else
                        {


                            ENCDraft encDraft = new ENCDraft(0,ENCAffected.getEditText().getText().toString(),ENCNMWeek.getEditText().getText().toString(),ENCNMYear.getEditText().getText().toString(),ENCLatitudeDegree.getEditText().getText().toString(),ENCLatitudeMinutes.getEditText().getText().toString(),ENCLatitudeDecimal.getEditText().getText().toString(),enclatitudepostion,ENCLongitudeDegrees.getEditText().getText().toString(),ENCLongitudeMinutes.getEditText().getText().toString(),ENCLongitudeDecimal.getEditText().getText().toString(),enclongitudeposition,ENCSupportingInformation.getEditText().getText().toString(),ENCfileURI);

                            ENCdatabase.getENCDraft().addENC(encDraft);
                        }



                        finish();
                        //Yes button clicked
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked


                        finish();
                        break;
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(ENCActivity.this);
        builder.setMessage("Would You Like To Save Draft?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(ENCcurrentPhotoPath);
                //selectedImage.setImageURI(Uri.fromFile(f));
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                //  f.getAbsolutePath();
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                Log.e("api", contentUri.toString());

                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                ENCfileURI.add(contentUri.toString());
                ENCrecycler.notifyDataSetChanged();
            }

        }

//        if (requestCode == GALLERY_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri contentUri = data.getData();
//                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//                String imageFileName = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
//                Log.d("tag", "onActivityResult: Gallery Image Uri:  " + imageFileName);
//                selectedImage.setImageURI(contentUri);
//            }
//
//        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data.getClipData() != null) {

                int totalItemsSelected = data.getClipData().getItemCount();

                for (int i = 0; i < totalItemsSelected; i++) {

                    Uri fileUri = data.getClipData().getItemAt(i).getUri();

                    String fileName = getFileName(fileUri);

                    ENCfileURI.add(fileUri.toString());
                    ENCrecycler.notifyDataSetChanged();
                    Log.e("tagg", ENCfileURI.get(i));
//                    fileNameList.add(fileName);
//                    fileDoneList.add("uploading");
//                    uploadListAdapter.notifyDataSetChanged();

//                 StorageReference fileToUpload = mStorage.child("Images").child(fileName);
////
//                    final int finalI = i;
//                    fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
////                            fileDoneList.remove(finalI);
////                            fileDoneList.add(finalI, "done");
////
////                            uploadListAdapter.notifyDataSetChanged();
//                            Toast.makeText(ChartActivity.this, "done", Toast.LENGTH_SHORT).show();
////
//                      }
//                    });

                }

                //Toast.makeText(MainActivity.this, "Selected Multiple Files", Toast.LENGTH_SHORT).show();

            } else if (data.getData() != null) {

                Uri im = data.getData();
                ENCfileURI.add(im.toString());
                ENCrecycler.notifyDataSetChanged();
         //       Toast.makeText(ENCActivity.this, "Selected Single File", Toast.LENGTH_SHORT).show();

            }

        }
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);
                    //    txtAddress.setText("Address: "+address);
                    //    txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                    // Toast.makeText(this, ""+selectedLatitude+""+selectedLongitude, Toast.LENGTH_SHORT).show();
                    getFormattedLocationInDegree(selectedLatitude, selectedLongitude);
                    Log.e("taging",selectedLatitude+""+selectedLongitude);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }


    private void askCameraPermissions() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        } else {
            dispatchTakePictureIntent();
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        ENCcurrentPhotoPath = image.getAbsolutePath();
        Log.e("current", ENCcurrentPhotoPath);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
     //   Toast.makeText(this, "asd", Toast.LENGTH_SHORT).show();
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
      //      Toast.makeText(this, "asdasdasddasdasd", Toast.LENGTH_SHORT).show();
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.hydrochart.hydrochart.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public  String getFormattedLocationInDegree(double latitude, double longitude) {
        try {

            int latSeconds = (int) Math.round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int) Math.round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            String latDegree = latDegrees >= 0 ? "N" : "S";
            String lonDegrees = longDegrees >= 0 ? "E" : "W";



            ENCLatitudeDegree.getEditText().setText(Math.abs(latDegrees)+"");
            ENCLatitudeMinutes.getEditText().setText(latMinutes+"");
            ENCLatitudeDecimal.getEditText().setText(latSeconds+"");

            ENCLongitudeDegrees.getEditText().setText(Math.abs(longDegrees)+"");
            ENCLongitudeMinutes.getEditText().setText(longMinutes+"");
            ENCLongitudeDecimal.getEditText().setText(longSeconds+"");

            if(latDegree.equals("N"))
            {
              //  Toast.makeText(this, "GOOD", Toast.LENGTH_SHORT).show();
                ENCspinner_latitude.setSelection(0);
            }
            if(latDegree.equals("S"))
            {
             //   Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                ENCspinner_latitude.setSelection(1);
            }
            if(lonDegrees.equals("E"))
            {
                ENCspinner_longtitude.setSelection(0);
            }

            if(lonDegrees.equals("W"))
            {
                ENCspinner_longtitude.setSelection(1);
            }


            Log.e("tagssss",Math.abs(latDegrees)+""+latMinutes+""+latSeconds+""+Math.abs(longDegrees)+""+longMinutes+""+longSeconds+"");

            return Math.abs(latDegrees) + "°" + latMinutes + "'" + latSeconds
                    + "\"" + latDegree + " " + Math.abs(longDegrees) + "°" + longMinutes
                    + "'" + longSeconds + "\"" + lonDegrees;
        } catch (Exception e) {
            return "" + String.format("%8.5f", latitude) + "  "
                    + String.format("%8.5f", longitude);
        }
    }





    private boolean validateENCAffected() {
        encaffected = ENCAffected.getEditText().getText().toString().trim();

        if (encaffected.isEmpty()) {
            ENCAffected.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCAffected.setError(null);
            return true;
        }
    }

    private boolean validateENCNMWeek() {
        encnmweek = ENCNMWeek.getEditText().getText().toString().trim();

        if (encnmweek.isEmpty()) {
            ENCNMWeek.setError("Field can't be empty");
            return false;
        }
        else if(Integer.parseInt(encnmweek)<=0||Integer.parseInt(encnmweek)>53)
        {
            ENCNMWeek.setError("Chart NM Week Should Be Between 1-53");
            return false;
        }

        else  {

            ENCNMWeek.setError(null);
            return true;
        }
    }

    private boolean validateENCNMYear() {
        encnmyear = ENCNMYear.getEditText().getText().toString().trim();
        // Toast.makeText(this, "chartdw"+chartnmyear, Toast.LENGTH_SHORT).show();
        int date= Calendar.getInstance().get(Calendar.YEAR);
        if (encnmyear.isEmpty()) {
            ENCNMYear.setError("Field can't be empty");

            return false;
        }
        else if(Integer.parseInt(encnmyear)<1700||Integer.parseInt(encnmyear)>date)
        {
            ENCNMYear.setError("Chart NM Year Should Be Between 1700-"+date);
            return false;
        }
        else  {

            ENCNMYear.setError(null);
            return true;
        }
    }

    private boolean validateENCLatitudeDegree() {
        enclatitudedegree = ENCLatitudeDegree.getEditText().getText().toString().trim();
        if (enclatitudedegree.isEmpty()) {
            ENCLatitudeDegree.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCLatitudeDegree.setError(null);
            return true;
        }
    }

    private boolean validateENCLatitudeMinutes() {
        enclatitudeminutes = ENCLatitudeMinutes.getEditText().getText().toString().trim();
        if (enclatitudeminutes.isEmpty()) {
            ENCLatitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCLatitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validateENCLatitudeDecimal() {
        enclatitudedecimal = ENCLatitudeDecimal.getEditText().getText().toString().trim();
        if (enclatitudedecimal.isEmpty()) {
            ENCLatitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCLatitudeDecimal.setError(null);
            return true;
        }
    }

    private boolean validateENCLongitudeDegree() {
        enclongitudedegrees = ENCLongitudeDegrees.getEditText().getText().toString().trim();
        if (enclongitudedegrees.isEmpty()) {
            ENCLongitudeDegrees.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCLongitudeDegrees.setError(null);
            return true;
        }
    }

    private boolean validateENCLongitudeMinutes() {
        enclongtitugeminutes =  ENCLongitudeMinutes.getEditText().getText().toString().trim();
        if (enclongtitugeminutes.isEmpty()) {
            ENCLongitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCLongitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validateENCLongitudeDecimal() {
        enclongitudedecimal = ENCLongitudeDecimal.getEditText().getText().toString().trim();
        if (enclongitudedecimal.isEmpty()) {
            ENCLongitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            ENCLongitudeDecimal.setError(null);
            return true;
        }
    }

}
