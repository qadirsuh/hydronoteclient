package com.hydrochart.hydrochart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hydrochart.hydrochart.Databases.DetailsDatabase;
import com.hydrochart.hydrochart.Databases.ENCDraft;
import com.hydrochart.hydrochart.Databases.PublicationDraft;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PublicationActivity extends AppCompatActivity {
    private static final int ADDRESS_PICKER_REQUEST = 1000;
    Toolbar toolbar;
    Button PUBLICATIONCamera,PUBLICATIONGallery, PUBLICATIONlocationPicker,PUBLICATIONSubmit;
   private RecyclerView recyclerView;
    List<String> PUBLICATIONfileURI;
    List<String> PUBLICATIONdownloadFilePath;
    Recycler PUBLICATIONrecycler;
    String PUBLICATIONcurrentPhotoPath;
    DetailsDatabase PUBLICATIONdatabase;
    private Spinner PUBLICATIONspinner_latitude,PUBLICATIONspinner_longtitude;


    public TextInputLayout PUBLICATIONAffected,PUBLICATIONNumber, PUBLICATIONNMWeek, PUBLICATIONNMYear, PUBLICATIONLatitudeDegree, PUBLICATIONLatitudeMinutes, PUBLICATIONLatitudeDecimal;
    TextInputLayout PUBLICATIONLongitudeDegrees, PUBLICATIONLongitudeMinutes,PUBLICATIONLongitudeDecimal,PUBLICATIONSupportingInformation;

    String publicationaffected,publicationnumber,publicationnmweek,publicationnmyear,publicationlatitudedegree,publicationlatitudeminutes,publicationlatitudedecimal,publicationlongitudedegrees,publicationlongtitugeminutes,publicationlongitudedecimal;
    String publicationlatitudepostion,publicationlongitudeposition;
    String userEmail,userName,userCompany,userOccupation,userHomeCountry,userVesselName,userIMONumber;
    String downloadPath;
    String saveCurrentDate,saveCurrentTime,RandomKEY;
    String publicationsupportinginformation;
    private ProgressDialog Progress;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int GALLERY_REQUEST_CODE = 105;
    // private StorageReference mStorage;
    private StorageReference publicationImageRef;
    private DatabaseReference publicationdataRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_publication);

        toolbar = findViewById(R.id.toolbarenc);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        PUBLICATIONCamera = findViewById(R.id.publicationCamera);
        PUBLICATIONGallery = findViewById(R.id.publicationGallary);
        PUBLICATIONlocationPicker = findViewById(R.id.publicationUseLocationPicker);
        PUBLICATIONSubmit=findViewById(R.id.submitpublicationDetails);
        recyclerView = findViewById(R.id.publicationRecycler);
        PUBLICATIONfileURI = new ArrayList<>();
        PUBLICATIONrecycler = new Recycler(this,   PUBLICATIONfileURI);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(  PUBLICATIONrecycler);
        //   mStorage = FirebaseStorage.getInstance().getReference();
        PUBLICATIONdownloadFilePath=new ArrayList<>();
        publicationImageRef = FirebaseStorage.getInstance().getReference().child("Images");
        publicationdataRef = FirebaseDatabase.getInstance().getReference().child("PublicationDetails");
        Progress = new ProgressDialog(this);
        MapUtility.apiKey = getResources().getString(R.string.your_api_key);
        PUBLICATIONdatabase = Room.databaseBuilder(this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

        PUBLICATIONAffected=findViewById(R.id.publicationAffected);
        PUBLICATIONNumber=findViewById(R.id.publicationPageNumber);
        PUBLICATIONNMWeek=findViewById(R.id.publicationNMWeek);
        PUBLICATIONNMYear=findViewById(R.id.publicationNMYear);
        PUBLICATIONLatitudeDegree=findViewById(R.id.publicationLatitudeDegree);
        PUBLICATIONLatitudeMinutes=findViewById(R.id.publicationLatitudeMinutes);
        PUBLICATIONLatitudeDecimal=findViewById(R.id.publicationLatitudeDecimal);
        PUBLICATIONLongitudeDegrees=findViewById(R.id.publicationLongitudeDegrees);
        PUBLICATIONLongitudeMinutes=findViewById(R.id.publicationLongitudeMinutes);
        PUBLICATIONLongitudeDecimal=findViewById(R.id.publicationLongitudeDecimal);
        PUBLICATIONSupportingInformation=findViewById(R.id.publicationSupportingInformation);
        PUBLICATIONspinner_latitude=findViewById(R.id.publicationspinner_latitude);
        PUBLICATIONspinner_longtitude=findViewById(R.id.publicationspinner_longitude);

        List<PublicationDraft> publicationDrafts=new ArrayList<>();

        publicationDrafts.addAll(PUBLICATIONdatabase.getPublicationDraft().getPublicationDraft());


        userName = PUBLICATIONdatabase.getDetails().getDetails().get(0).getUserName();
        userEmail=   PUBLICATIONdatabase.getDetails().getDetails().get(0).getEmail();
        userCompany = PUBLICATIONdatabase.getDetails().getDetails().get(0).getCompany();
        userOccupation = PUBLICATIONdatabase.getDetails().getDetails().get(0).getOccupation();
        userHomeCountry=  PUBLICATIONdatabase.getDetails().getDetails().get(0).getHomeCountry();
        userVesselName= PUBLICATIONdatabase.getDetails().getDetails().get(0).getVesselName();
        userIMONumber= PUBLICATIONdatabase.getDetails().getDetails().get(0).getImoNumber();

        PUBLICATIONSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkChartInput();
            }
        });

        PUBLICATIONCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCameraPermissions();

            }
        });


        PUBLICATIONGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
            }
        });


        PUBLICATIONlocationPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PublicationActivity.this, LocationPickerActivity.class);
                startActivityForResult(i, ADDRESS_PICKER_REQUEST);
            }
        });


        ArrayAdapter<CharSequence> latitude = ArrayAdapter.createFromResource(this,R.array.NS,android.R.layout.simple_spinner_item);

        latitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        PUBLICATIONspinner_latitude.setAdapter(latitude);
        PUBLICATIONspinner_latitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        publicationlatitudepostion="N";
                        break;
                    case 1:
                       publicationlatitudepostion="S";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> longitude = ArrayAdapter.createFromResource(this,R.array.EW,android.R.layout.simple_spinner_item);

        longitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        PUBLICATIONspinner_longtitude.setAdapter(longitude);
        PUBLICATIONspinner_longtitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        publicationlongitudeposition="E";
                        break;
                    case 1:
                        publicationlongitudeposition="W";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Intent intent = getIntent();

        String publicationdraft="NO";
        int selector=intent.getIntExtra("PUBLICATIONDraft",0);

        //Toast.makeText(this, chartdraft+"", Toast.LENGTH_SHORT).show();
        if(selector==1)
        {

            if(publicationDrafts.size()>0)
            {


            //Toast.makeText(this, "DONEEE"+"sdadasd"+database.getChart().getChartDraft().get(0).getLatitudeDecimal(), Toast.LENGTH_SHORT).show();
           PUBLICATIONAffected.getEditText().setText(publicationDrafts.get(0).getPublicationAffected());
            PUBLICATIONNumber.getEditText().setText(publicationDrafts.get(0).getPageNumber());
            PUBLICATIONNMWeek.getEditText().setText(publicationDrafts.get(0).getNMWeek());
            PUBLICATIONNMYear.getEditText().setText(publicationDrafts.get(0).getNMYear());
            PUBLICATIONLatitudeDegree.getEditText().setText(publicationDrafts.get(0).getLatitudeDegree());
            PUBLICATIONLatitudeMinutes.getEditText().setText(publicationDrafts.get(0).getLatitudeMinutes());
            PUBLICATIONLatitudeDecimal.getEditText().setText(publicationDrafts.get(0).getLatitudeDecimal());
            String latitudeP=publicationDrafts.get(0).getLatitudePosition();

            Toast.makeText(this, latitudeP+"", Toast.LENGTH_SHORT).show();
            if(latitudeP.equals("N"))
            {
                //  Toast.makeText(this, "N", Toast.LENGTH_SHORT).show();
                PUBLICATIONspinner_latitude.setSelection(0);
            }
            if(latitudeP.equals("S")){
                //  Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                PUBLICATIONspinner_latitude.setSelection(1);
            }
            PUBLICATIONLongitudeDegrees.getEditText().setText(publicationDrafts.get(0).getLongitudeDegree());
            PUBLICATIONLongitudeMinutes.getEditText().setText(publicationDrafts.get(0).getLongitudeMinutes());
            PUBLICATIONLongitudeDecimal.getEditText().setText(publicationDrafts.get(0).getLongitudeDecimal());
            String longitudeP=publicationDrafts.get(0).getLongitudePosition();
            Toast.makeText(this, longitudeP+"", Toast.LENGTH_SHORT).show();

            if(longitudeP.equals("E"))
            {
                //     Toast.makeText(this, "E", Toast.LENGTH_SHORT).show();
                PUBLICATIONspinner_longtitude.setSelection(0);
            }
            if(longitudeP.equals("W")){
                //    Toast.makeText(this, "W", Toast.LENGTH_SHORT).show();
                PUBLICATIONspinner_longtitude.setSelection(1);
            }


            PUBLICATIONSupportingInformation.getEditText().setText(publicationDrafts.get(0).getObservation());
            for(int i=0;i<publicationDrafts.get(0).getURLIMAGE().size();i++)
            {

                if(!publicationDrafts.get(0).getURLIMAGE().get(i).contains("content:"))
                {
                    PUBLICATIONfileURI.add(publicationDrafts.get(0).getURLIMAGE().get(i));
                }
                Log.e("img",publicationDrafts.get(0).getURLIMAGE().get(i));

            }
            PUBLICATIONrecycler.notifyDataSetChanged();

        }

    }   }


    private void checkChartInput() {

        if(!validatePUBLICATIONAffected()|!validatePUBLICATIONNumber()|!validatePUBLICATIONNMWeek()|!validatePUBLICATIONNMYear()|!validatePUBLICATIONLatitudeDegree()|!validatePUBLICATIONLatitudeMinutes()|!validatePUBLICATIONLatitudeDecimal()|!validatePUBLICATIONLongitudeDegree()|!validatePUBLICATIONLongitudeMinutes()|!validatePUBLICATIONLongitudeDecimal())
        {
            return;
        }

        publicationsupportinginformation = PUBLICATIONSupportingInformation.getEditText().getText().toString().trim();



     //   Toast.makeText(this, "DONE", Toast.LENGTH_SHORT).show();
        Progress.setTitle("Uploading Information..");
        Progress.setMessage("PLEASE WAIT WE ARE UPLOADING INFORMATION..");
        Progress.setCanceledOnTouchOutside(false);
        Progress.show();
        SaveData();
    }

    private void SaveData() {
        PUBLICATIONdownloadFilePath.clear();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a"); // it will give us the currentTime a means am and pm
        saveCurrentTime = currentTime.format(calendar.getTime());
        RandomKEY = saveCurrentDate + saveCurrentTime;
        if (PUBLICATIONfileURI.size() > 0) {
            for (int i = 0; i < PUBLICATIONfileURI.size(); i++) {
                final StorageReference filePath;
                filePath = publicationImageRef.child(Uri.parse(PUBLICATIONfileURI.get(i)).getLastPathSegment() + RandomKEY + ".jpg"); //last path segment is used to get the imageuri last / part which is basically image name in the uri
                final UploadTask uploadTask = filePath.putFile(Uri.parse(PUBLICATIONfileURI.get(i)));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String messege = e.toString();
                        Progress.dismiss();
                        Toast.makeText(PublicationActivity.this, "Error: " + messege + "..", Toast.LENGTH_SHORT).show();

                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { // tasksnapshot show about the state of the running upload task
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //       Toast.makeText(ChartActivity.this, "Image Stored Successfully..", Toast.LENGTH_SHORT).show();

                        Task<Uri> taskUrl = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {

                                    throw task.getException();
                                }
                                downloadPath = filePath.getDownloadUrl().toString();

                                return filePath.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {

                                    downloadPath = task.getResult().toString();
                                    Log.e("image", downloadPath);
                                    //Toast.makeText(ChartActivity.this, downloadPath + "", Toast.LENGTH_SHORT).show();
                                    PUBLICATIONdownloadFilePath.add(downloadPath);
                                    // Toast.makeText(ChartActivity.this, "Product Image URL Got Successfully..", Toast.LENGTH_SHORT).show();
                                    // SaveProductInfoToDatabase();
                                    Log.e("TAGINGINGIG",PUBLICATIONdownloadFilePath.size()+"  "+ PUBLICATIONfileURI.size());
                                    if (PUBLICATIONdownloadFilePath.size() == PUBLICATIONfileURI.size()) {
                                        SaveProductInfoToPUBLICATION();
                                    }
                                }
                            }
                        });
                    }
                });

            }
            Log.e("calling", "work");

            //    SaveProductInfoToChart();
        }

        else
        {

            HashMap<String ,Object> hashMap= new HashMap<>();
            // hashMap.put("image",downloadFilePath.get(0));
            hashMap.put("UserName",userName);
            hashMap.put("UserEmail",userEmail);
            hashMap.put("UserCompany",userCompany);
            hashMap.put("UserOccupation",userOccupation);
            hashMap.put("UserHomeCountry",userHomeCountry);
            hashMap.put("UserVesselName",userVesselName);
            hashMap.put("UserIMONumber",userIMONumber);
            hashMap.put("PUBLICATIONAffected",publicationaffected);
            hashMap.put("PUBLICATIONNumber",publicationnumber);
            hashMap.put("PUBLICATIONNMWeek",publicationnmweek);
            hashMap.put("PUBLICATIONNMYear",publicationnmyear);
            hashMap.put("PUBLICATIONLatitudeDegree",publicationlatitudedegree);
            hashMap.put("PUBLICATIONLatitudeMinutes",publicationlatitudeminutes);
            hashMap.put("PUBLICATIONLatitudeDecimals",publicationlatitudedecimal);
            hashMap.put("PUBLICATIONLatitudePosition",publicationlatitudepostion);
            hashMap.put("PUBLICATIONLongitudeDegree",publicationlongitudedegrees);
            hashMap.put("PUBLICATIONLongitudeMinutes",publicationlongtitugeminutes);
            hashMap.put("PUBLICATIONLongitudeDecimals",publicationlongitudedecimal);
            hashMap.put("PUBLICATIONLongitudePosition",publicationlongitudeposition);
            hashMap.put("PUBLICATIONSupportingInformation",publicationsupportinginformation);
            hashMap.put("date",saveCurrentDate);
            hashMap.put("time",saveCurrentTime);






            publicationdataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {

                        Toast.makeText(PublicationActivity.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                        Progress.dismiss();
                    }
                    else {
                        //   loginProgress.dismiss();
                        String messege =task.getException().toString();
                        Progress.dismiss();
                        Toast.makeText(PublicationActivity.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


    }

    private void SaveProductInfoToPUBLICATION() {
        HashMap<String ,Object> hashMap= new HashMap<>();
        // hashMap.put("image",downloadFilePath.get(0));
        for(int i=0;i<PUBLICATIONdownloadFilePath.size();i++)
        {
            hashMap.put("image"+i,PUBLICATIONdownloadFilePath.get(i));
        }
        hashMap.put("UserName",userName);
        hashMap.put("UserEmail",userEmail);
        hashMap.put("UserCompany",userCompany);
        hashMap.put("UserOccupation",userOccupation);
        hashMap.put("UserHomeCountry",userHomeCountry);
        hashMap.put("UserVesselName",userVesselName);
        hashMap.put("UserIMONumber",userIMONumber);
        hashMap.put("PUBLICATIONAffected",publicationaffected);
        hashMap.put("PUBLICATIONNumber",publicationnumber);
        hashMap.put("PUBLICATIONNMWeek",publicationnmweek);
        hashMap.put("PUBLICATIONNMYear",publicationnmyear);
        hashMap.put("PUBLICATIONLatitudeDegree",publicationlatitudedegree);
        hashMap.put("PUBLICATIONLatitudeMinutes",publicationlatitudeminutes);
        hashMap.put("PUBLICATIONLatitudeDecimals",publicationlatitudedecimal);
        hashMap.put("PUBLICATIONLatitudePosition",publicationlatitudepostion);
        hashMap.put("PUBLICATIONLongitudeDegree",publicationlongitudedegrees);
        hashMap.put("PUBLICATIONLongitudeMinutes",publicationlongtitugeminutes);
        hashMap.put("PUBLICATIONLongitudeDecimals",publicationlongitudedecimal);
        hashMap.put("PUBLICATIONLongitudePosition",publicationlongitudeposition);
        hashMap.put("PUBLICATIONSupportingInformation",publicationsupportinginformation);
        hashMap.put("date",saveCurrentDate);
        hashMap.put("time",saveCurrentTime);






        publicationdataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Progress.dismiss();
                    Toast.makeText(PublicationActivity.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    //   loginProgress.dismiss();
                    Progress.dismiss();
                    String messege =task.getException().toString();
                    Toast.makeText(PublicationActivity.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERM_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        PUBLICATIONdatabase = Room.databaseBuilder(PublicationActivity.this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

                        List<PublicationDraft> publicationDrafts = new ArrayList<>();

                        publicationDrafts.addAll(PUBLICATIONdatabase.getPublicationDraft().getPublicationDraft());


                       //   Log.e("SIZEING",publicationDrafts.size()+"");
                        if(publicationDrafts.size()>0) {

                          //  Toast.makeText(PublicationActivity.this, "UPDATE", Toast.LENGTH_SHORT).show();
                            PublicationDraft publicationDraft = new PublicationDraft(0,PUBLICATIONAffected.getEditText().getText().toString(),PUBLICATIONNumber.getEditText().getText().toString(),PUBLICATIONNMWeek.getEditText().getText().toString(),PUBLICATIONNMYear.getEditText().getText().toString(),PUBLICATIONLatitudeDegree.getEditText().getText().toString(),PUBLICATIONLatitudeMinutes.getEditText().getText().toString(),PUBLICATIONLatitudeDecimal.getEditText().getText().toString(),publicationlatitudepostion,PUBLICATIONLongitudeDegrees.getEditText().getText().toString(),PUBLICATIONLongitudeMinutes.getEditText().getText().toString(),PUBLICATIONLongitudeDecimal.getEditText().getText().toString(),publicationlongitudeposition,PUBLICATIONSupportingInformation.getEditText().getText().toString(),PUBLICATIONfileURI);

                            PUBLICATIONdatabase.getPublicationDraft().updatePublication(publicationDraft);
                        }
                        else
                        {


                            PublicationDraft publicationDraft = new PublicationDraft(0,PUBLICATIONAffected.getEditText().getText().toString(),PUBLICATIONNumber.getEditText().getText().toString(),PUBLICATIONNMWeek.getEditText().getText().toString(),PUBLICATIONNMYear.getEditText().getText().toString(),PUBLICATIONLatitudeDegree.getEditText().getText().toString(),PUBLICATIONLatitudeMinutes.getEditText().getText().toString(),PUBLICATIONLatitudeDecimal.getEditText().getText().toString(),publicationlatitudepostion,PUBLICATIONLongitudeDegrees.getEditText().getText().toString(),PUBLICATIONLongitudeMinutes.getEditText().getText().toString(),PUBLICATIONLongitudeDecimal.getEditText().getText().toString(),publicationlongitudeposition,PUBLICATIONSupportingInformation.getEditText().getText().toString(),PUBLICATIONfileURI);
                            PUBLICATIONdatabase.getPublicationDraft().addPublication(publicationDraft);
                        }



                        finish();
                        //Yes button clicked
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked


                        finish();
                        break;
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(PublicationActivity.this);
        builder.setMessage("Would You Like To Save Draft?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(PUBLICATIONcurrentPhotoPath);
                //selectedImage.setImageURI(Uri.fromFile(f));
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                //  f.getAbsolutePath();
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                Log.e("api", contentUri.toString());

                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                PUBLICATIONfileURI.add(contentUri.toString());
                PUBLICATIONrecycler.notifyDataSetChanged();
            }

        }

//        if (requestCode == GALLERY_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri contentUri = data.getData();
//                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//                String imageFileName = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
//                Log.d("tag", "onActivityResult: Gallery Image Uri:  " + imageFileName);
//                selectedImage.setImageURI(contentUri);
//            }
//
//        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data.getClipData() != null) {

                int totalItemsSelected = data.getClipData().getItemCount();

                for (int i = 0; i < totalItemsSelected; i++) {

                    Uri fileUri = data.getClipData().getItemAt(i).getUri();

                    String fileName = getFileName(fileUri);

                    PUBLICATIONfileURI.add(fileUri.toString());
                    PUBLICATIONrecycler.notifyDataSetChanged();
                    Log.e("tagg",PUBLICATIONfileURI.get(i));
//                    fileNameList.add(fileName);
//                    fileDoneList.add("uploading");
//                    uploadListAdapter.notifyDataSetChanged();

//                 StorageReference fileToUpload = mStorage.child("Images").child(fileName);
////
//                    final int finalI = i;
//                    fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
////                            fileDoneList.remove(finalI);
////                            fileDoneList.add(finalI, "done");
////
////                            uploadListAdapter.notifyDataSetChanged();
//                            Toast.makeText(ChartActivity.this, "done", Toast.LENGTH_SHORT).show();
////
//                      }
//                    });

                }

                //Toast.makeText(MainActivity.this, "Selected Multiple Files", Toast.LENGTH_SHORT).show();

            } else if (data.getData() != null) {

                Uri im = data.getData();
                PUBLICATIONfileURI.add(im.toString());
                PUBLICATIONrecycler.notifyDataSetChanged();
             //   Toast.makeText(PublicationActivity.this, "Selected Single File", Toast.LENGTH_SHORT).show();

            }

        }
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);
                    //    txtAddress.setText("Address: "+address);
                    //    txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                    // Toast.makeText(this, ""+selectedLatitude+""+selectedLongitude, Toast.LENGTH_SHORT).show();
                    getFormattedLocationInDegree(selectedLatitude, selectedLongitude);
                    Log.e("taging",selectedLatitude+""+selectedLongitude);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }


    private void askCameraPermissions() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        } else {
            dispatchTakePictureIntent();
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        PUBLICATIONcurrentPhotoPath = image.getAbsolutePath();
        Log.e("current", PUBLICATIONcurrentPhotoPath);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
     //   Toast.makeText(this, "asd", Toast.LENGTH_SHORT).show();
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
       //     Toast.makeText(this, "asdasdasddasdasd", Toast.LENGTH_SHORT).show();
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.hydrochart.hydrochart.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public  String getFormattedLocationInDegree(double latitude, double longitude) {
        try {

            int latSeconds = (int) Math.round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int) Math.round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            String latDegree = latDegrees >= 0 ? "N" : "S";
            String lonDegrees = longDegrees >= 0 ? "E" : "W";



            PUBLICATIONLatitudeDegree.getEditText().setText(Math.abs(latDegrees)+"");
            PUBLICATIONLatitudeMinutes.getEditText().setText(latMinutes+"");
            PUBLICATIONLatitudeDecimal.getEditText().setText(latSeconds+"");

            PUBLICATIONLongitudeDegrees.getEditText().setText(Math.abs(longDegrees)+"");
            PUBLICATIONLongitudeMinutes.getEditText().setText(longMinutes+"");
            PUBLICATIONLongitudeDecimal.getEditText().setText(longSeconds+"");

            if(latDegree.equals("N"))
            {
              //  Toast.makeText(this, "GOOD", Toast.LENGTH_SHORT).show();
                PUBLICATIONspinner_latitude.setSelection(0);
            }
            if(latDegree.equals("S"))
            {
            //    Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                PUBLICATIONspinner_latitude.setSelection(1);
            }
            if(lonDegrees.equals("E"))
            {
                PUBLICATIONspinner_longtitude.setSelection(0);
            }

            if(lonDegrees.equals("W"))
            {
                PUBLICATIONspinner_longtitude.setSelection(1);
            }


            Log.e("tagssss",Math.abs(latDegrees)+""+latMinutes+""+latSeconds+""+Math.abs(longDegrees)+""+longMinutes+""+longSeconds+"");

            return Math.abs(latDegrees) + "°" + latMinutes + "'" + latSeconds
                    + "\"" + latDegree + " " + Math.abs(longDegrees) + "°" + longMinutes
                    + "'" + longSeconds + "\"" + lonDegrees;
        } catch (Exception e) {
            return "" + String.format("%8.5f", latitude) + "  "
                    + String.format("%8.5f", longitude);
        }
    }





    private boolean validatePUBLICATIONAffected() {
       publicationaffected = PUBLICATIONAffected.getEditText().getText().toString().trim();

        if (publicationaffected.isEmpty()) {
            PUBLICATIONAffected.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONAffected.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONNumber() {
        publicationnumber = PUBLICATIONNumber.getEditText().getText().toString().trim();

        if (publicationnumber.isEmpty()) {
            PUBLICATIONNumber.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONNumber.setError(null);
            return true;
        }
    }


    private boolean validatePUBLICATIONNMWeek() {
        publicationnmweek = PUBLICATIONNMWeek.getEditText().getText().toString().trim();

        if (publicationnmweek.isEmpty()) {
            PUBLICATIONNMWeek.setError("Field can't be empty");
            return false;
        }
        else if(Integer.parseInt(publicationnmweek)<=0||Integer.parseInt(publicationnmweek)>53)
        {
            PUBLICATIONNMWeek.setError("Chart NM Week Should Be Between 1-53");
            return false;
        }

        else  {

            PUBLICATIONNMWeek.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONNMYear() {
        publicationnmyear = PUBLICATIONNMYear.getEditText().getText().toString().trim();
        // Toast.makeText(this, "chartdw"+chartnmyear, Toast.LENGTH_SHORT).show();
        int date= Calendar.getInstance().get(Calendar.YEAR);
        if (publicationnmyear.isEmpty()) {
            PUBLICATIONNMYear.setError("Field can't be empty");

            return false;
        }
        else if(Integer.parseInt(publicationnmyear)<1700||Integer.parseInt(publicationnmyear)>date)
        {
            PUBLICATIONNMYear.setError("Chart NM Year Should Be Between 1700-"+date);
            return false;
        }
        else  {

            PUBLICATIONNMYear.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONLatitudeDegree() {
        publicationlatitudedegree = PUBLICATIONLatitudeDegree.getEditText().getText().toString().trim();
        if (publicationlatitudedegree.isEmpty()) {
            PUBLICATIONLatitudeDegree.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONLatitudeDegree.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONLatitudeMinutes() {
       publicationlatitudeminutes = PUBLICATIONLatitudeMinutes.getEditText().getText().toString().trim();
        if (publicationlatitudeminutes.isEmpty()) {
            PUBLICATIONLatitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONLatitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONLatitudeDecimal() {
        publicationlatitudedecimal =  PUBLICATIONLatitudeDecimal.getEditText().getText().toString().trim();
        if (publicationlatitudedecimal.isEmpty()) {
            PUBLICATIONLatitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONLatitudeDecimal.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONLongitudeDegree() {
        publicationlongitudedegrees = PUBLICATIONLongitudeDegrees.getEditText().getText().toString().trim();
        if (publicationlongitudedegrees.isEmpty()) {
            PUBLICATIONLongitudeDegrees.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONLongitudeDegrees.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONLongitudeMinutes() {
        publicationlongtitugeminutes =   PUBLICATIONLongitudeMinutes.getEditText().getText().toString().trim();
        if (publicationlongtitugeminutes.isEmpty()) {
            PUBLICATIONLongitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONLongitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validatePUBLICATIONLongitudeDecimal() {
        publicationlongitudedecimal = PUBLICATIONLongitudeDecimal.getEditText().getText().toString().trim();
        if (publicationlongitudedecimal.isEmpty()) {
            PUBLICATIONLongitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            PUBLICATIONLongitudeDecimal.setError(null);
            return true;
        }
    }

}
