package com.hydrochart.hydrochart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.room.Room;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.hydrochart.hydrochart.Databases.DetailsDatabase;
import com.hydrochart.hydrochart.Databases.DetailsEntity;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView details;
    TextInputLayout userName;
    TextInputLayout userEmail;
    TextInputLayout userCompany;
    TextInputLayout VesselName;
    TextInputLayout imoNumber;
    TextInputLayout occupation;
    TextInputLayout homeCountry;
    RadioButton yes,no;
    RadioGroup radioGroup;
    Button submit;
    DetailsDatabase database;
    ImageView setting;

    String usernameInput,emailInput,userCompanyInput,userVesselNameInput,userImoNumber,userHomeCountry,userOccupation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbara);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        userName = findViewById(R.id.userName);
        userEmail = findViewById(R.id.userEmail);
        userCompany =findViewById(R.id.userComapany);
        VesselName = findViewById(R.id.vesselName);
        imoNumber=findViewById(R.id.imoNumber);
        occupation = findViewById(R.id.occupation);
        homeCountry = findViewById(R.id.homeCountry);
        yes = findViewById(R.id.yesBtn);
        no = findViewById(R.id.noBtn);
        submit =findViewById(R.id.submitDetails);
        radioGroup=findViewById(R.id.radioGroup);
setting=findViewById(R.id.setting);

setting.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this,SettingActivity.class);
        startActivity(intent);
    }
});
        database = Room.databaseBuilder(this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() == R.id.yesBtn) {
                    occupation.setVisibility(View.GONE);
                    homeCountry.setVisibility(View.GONE);
                    VesselName.setVisibility(View.VISIBLE);
                    imoNumber.setVisibility(View.VISIBLE);
                }
                if (group.getCheckedRadioButtonId() == R.id.noBtn) {
                    occupation.setVisibility(View.VISIBLE);
                    homeCountry.setVisibility(View.VISIBLE);
                    VesselName.setVisibility(View.GONE);
                    imoNumber.setVisibility(View.GONE);
                }
            }
        });
        occupation.setVisibility(View.GONE);
        homeCountry.setVisibility(View.GONE);
        yes.setChecked(true);

        List<DetailsEntity> detailsEntityList2 = new ArrayList<>();

        detailsEntityList2.addAll(database.getDetails().getDetails());

       if(detailsEntityList2.size()>0)
       {
           userName.getEditText().setText(detailsEntityList2.get(0).getUserName());
           userCompany.getEditText().setText(detailsEntityList2.get(0).getCompany());
           userEmail.getEditText().setText(detailsEntityList2.get(0).getEmail());
           imoNumber.getEditText().setText(detailsEntityList2.get(0).getImoNumber());
           VesselName.getEditText().setText(detailsEntityList2.get(0).getVesselName());
           occupation.getEditText().setText(detailsEntityList2.get(0).getOccupation());
           homeCountry.getEditText().setText(detailsEntityList2.get(0).getHomeCountry());

           if(!occupation.getEditText().getText().toString().isEmpty())
           {
               no.setChecked(true);
           }

       }



        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<DetailsEntity> detailsEntityList = new ArrayList<>();

                detailsEntityList.addAll(database.getDetails().getDetails());

                if(detailsEntityList.size()>0)
                {
                    updateconfirmInput();

                }

                else {
                    confirmInput();

                }



            }

            private void confirmInput() {
                if(!validateEmail()|!validateUsername()|!validateUserComapany())
                {
                    return;
                }
                if(yes.isChecked())
                {
                    if(validateUserVesselName()&&validateUserImoNumber())
                    {
                    //    Toast.makeText(MainActivity.this, "dddd", Toast.LENGTH_SHORT).show();
                        DetailsEntity entity = new DetailsEntity(0,usernameInput,emailInput,userCompanyInput,userVesselNameInput,userImoNumber,"","",true);
                        database.getDetails().addDetails(entity);
                        Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);



                    }

                }
                else {
                  //  Toast.makeText(MainActivity.this, "asdasd", Toast.LENGTH_SHORT).show();
                    DetailsEntity entity = new DetailsEntity(0,usernameInput,emailInput,userCompanyInput,"","",userOccupation,userHomeCountry,true);
                    database.getDetails().addDetails(entity);

                    Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }



            }


            private void updateconfirmInput() {
               // Toast.makeText(MainActivity.this, "Yes", Toast.LENGTH_SHORT).show();
                if(!validateEmail()|!validateUsername()|!validateUserComapany())
                {
                    return;
                }
                if(yes.isChecked())
                {
                    if(validateUserVesselName()&&validateUserImoNumber())
                    {
                     //  Toast.makeText(MainActivity.this, "update", Toast.LENGTH_SHORT).show();
                        DetailsEntity entity = new DetailsEntity(0,usernameInput,emailInput,userCompanyInput,userVesselNameInput,userImoNumber,"","",true);
                        database.getDetails().updateDetails(entity);

                        Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }

                }
                else {
                    if(validateUserHomeCountry()|validateUserOccupation())
                    {

                 //   Toast.makeText(MainActivity.this, "withocl", Toast.LENGTH_SHORT).show();
                    DetailsEntity entity = new DetailsEntity(0,usernameInput,emailInput,userCompanyInput,"","",userOccupation,userHomeCountry,true);
                    database.getDetails().updateDetails(entity);

                        Intent intent = new Intent(MainActivity.this,HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    }


                }




            }
        });
    }

    private boolean validateEmail() {
        emailInput = userEmail.getEditText().getText().toString().trim();

        if (emailInput.isEmpty()) {
            userEmail.setError("Field can't be empty");
            return false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()) {
            userEmail.setError("Please enter a valid email address");
            return false;
        } else {
            userEmail.setError(null);
            return true;
        }
    }

    private boolean validateUsername() {
        usernameInput = userName.getEditText().getText().toString().trim();

        if (usernameInput.isEmpty()) {
            userName.setError("Field can't be empty");
            return false;
        }
        else {
            userName.setError(null);
            return true;
        }
    }
    private boolean validateUserComapany() {
       userCompanyInput = userCompany.getEditText().getText().toString().trim();

        if (userCompanyInput.isEmpty()) {
            userCompany.setError("Field can't be empty");
            return false;
        }
        else {
            userCompany.setError(null);
            return true;
        }
    }
    private boolean validateUserHomeCountry() {
        userHomeCountry = homeCountry.getEditText().getText().toString().trim();

        if (userHomeCountry.isEmpty()) {
            homeCountry.setError("Field can't be empty");
            return false;
        }
        else {
            homeCountry.setError(null);
            return true;
        }
    }

    private boolean validateUserOccupation() {
        userOccupation = occupation.getEditText().getText().toString().trim();

        if (userOccupation.isEmpty()) {
            occupation.setError("Field can't be empty");
            return false;
        }
        else {
            occupation.setError(null);
            return true;
        }
    }



    private boolean validateUserVesselName() {
         userVesselNameInput = VesselName.getEditText().getText().toString().trim();

        if (userVesselNameInput.isEmpty()) {
            VesselName.setError("Field can't be empty");
            return false;
        }


        else  {

            VesselName.setError(null);
            return true;
        }
    }
    private boolean validateUserImoNumber() {
         userImoNumber = imoNumber.getEditText().getText().toString().trim();
      //  Toast.makeText(this, userImoNumber+"", Toast.LENGTH_SHORT).show();
        if (userImoNumber.isEmpty()) {
            imoNumber.setError("Field can't be empty");
            return false;
        }
        else if(!userImoNumber.contains("IMO")&&!userImoNumber.contains("imo"))
        {
        imoNumber.setError("IMO Number should be of the format IMO1234567");
            return false;
        }
        else {
            imoNumber.setError(null);
            return true;
        }
    }

}
