package com.hydrochart.hydrochart;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.hydrochart.hydrochart.Databases.DetailsDatabase;
import com.hydrochart.hydrochart.Databases.PublicationDraft;
import com.hydrochart.hydrochart.Databases.portinformationDraft;
import com.shivtechs.maplocationpicker.LocationPickerActivity;
import com.shivtechs.maplocationpicker.MapUtility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PortInformation extends AppCompatActivity {

    private static final int ADDRESS_PICKER_REQUEST = 1000;
    Toolbar toolbar;
    Button PORTINFORMATIONCamera,PORTINFORMATIONGallery, PORTINFORMATIONlocationPicker,PORTINFORMATIONSubmit;
        RecyclerView recyclerView;
    List<String> PORTINFORMATIONfileURI;
    List<String> PORTINFORMATIONdownloadFilePath;
    Recycler PORTINFORMATIONrecycler;
    String PORTINFORMATIONcurrentPhotoPath;
    DetailsDatabase PORTINFORMATIONdatabase;
    private Spinner PORTINFORMATIONspinner_latitude,PORTINFORMATIONspinner_longtitude;


    public TextInputLayout PORTINFORMATIONNameOfPort,PORTINFORMATIONCountry, PORTINFORMATIONLatitudeDegree, PORTINFORMATIONLatitudeMinutes, PORTINFORMATIONLatitudeDecimal;
    TextInputLayout PORTINFORMATIONLongitudeDegrees, PORTINFORMATIONLongitudeMinutes,PORTINFORMATIONLongitudeDecimal,PORTINFORMATIONSupportingInformation;
    TextInputLayout PORTINFORMATIONAnchorages,PORTINFORMATIONPilotage,PORTINFORMATIONDirections,PORTINFORMATIONTug,PORTINFORMATIONWharves,PORTINFORMATIONCargoHandling,PORTINFORMATIONRepairAndRescue;
    TextInputLayout PORTINFORMATIONSupply,PORTINFORMATIONServices,PORTINFORMATIONCommunication,PORTINFORMATIONPortAuthority,PORTINFORMATIONView;


    String portinformationanchorages,portinformationpilotage,portinformationdirections,portinformationtug,portinformationwharves,portinformationcargohandling,portinformationrepairandrescue;
    String portinformationsupply,portinformationservices,portinformationcommunication,portinformationportauthority,portinformationview;
    String portinformationnameofport,portinformationcountry,portinformationlatitudedegree,portinformationlatitudeminutes,portinformationlatitudedecimal,portinformationlongitudedegrees,portinformationlongtitugeminutes,portinformationlongitudedecimal;
    String portinformationlatitudepostion,portinformationlongitudeposition;
    String userEmail,userName,userCompany,userOccupation,userHomeCountry,userVesselName,userIMONumber;
    String downloadPath;
    String saveCurrentDate,saveCurrentTime,RandomKEY;
    String portinformationsupportinginformation;
    private ProgressDialog Progress;
    public static final int CAMERA_PERM_CODE = 101;
    public static final int CAMERA_REQUEST_CODE = 102;
    public static final int GALLERY_REQUEST_CODE = 105;
    // private StorageReference mStorage;
    private StorageReference portinformationImageRef;
    private DatabaseReference portinformationdataRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_port_information);

        toolbar = findViewById(R.id.toolbarenc);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        PORTINFORMATIONCamera = findViewById(R.id.portinformationCamera);
        PORTINFORMATIONGallery = findViewById(R.id.portinformationGallary);
        PORTINFORMATIONlocationPicker = findViewById(R.id.portinformationUseLocationPicker);
        PORTINFORMATIONSubmit=findViewById(R.id.submitportinformationDetails);
        recyclerView = findViewById(R.id.portinformationRecycler);
        PORTINFORMATIONfileURI = new ArrayList<>();
        PORTINFORMATIONrecycler = new Recycler(this,  PORTINFORMATIONfileURI);
        recyclerView.setLayoutManager(new GridLayoutManager(this,2));
        recyclerView.setAdapter( PORTINFORMATIONrecycler);
        //   mStorage = FirebaseStorage.getInstance().getReference();
        PORTINFORMATIONdownloadFilePath=new ArrayList<>();
        portinformationImageRef = FirebaseStorage.getInstance().getReference().child("Images");
        portinformationdataRef = FirebaseDatabase.getInstance().getReference().child("PortInformationDetails");
        Progress = new ProgressDialog(this);
        MapUtility.apiKey = getResources().getString(R.string.your_api_key);
        PORTINFORMATIONdatabase = Room.databaseBuilder(this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

        PORTINFORMATIONNameOfPort=findViewById(R.id.portinformationNameOfPort);
        PORTINFORMATIONCountry=findViewById(R.id.portinformationCountry);
        PORTINFORMATIONAnchorages=findViewById(R.id.portinformationAnchorages);
        PORTINFORMATIONPilotage=findViewById(R.id.portinformationPilotage);
        PORTINFORMATIONDirections=findViewById(R.id.portinformationDirections);
        PORTINFORMATIONTug=findViewById(R.id.portinformationTug);
        PORTINFORMATIONWharves=findViewById(R.id.portinformationWharves);
        PORTINFORMATIONCargoHandling=findViewById(R.id.portinformationCargoHandling);
        PORTINFORMATIONRepairAndRescue=findViewById(R.id.portinformationRapair);
        PORTINFORMATIONSupply=findViewById(R.id.portinformationSupply);
        PORTINFORMATIONServices=findViewById(R.id.portinformationServices);
        PORTINFORMATIONCommunication=findViewById(R.id.portinformationCommunications);
        PORTINFORMATIONPortAuthority=findViewById(R.id.portinformationPortAuthority);
        PORTINFORMATIONView=findViewById(R.id.portinformationView);
        PORTINFORMATIONLatitudeDegree=findViewById(R.id.portinformationLatitudeDegree);
        PORTINFORMATIONLatitudeMinutes=findViewById(R.id.portinformationLatitudeMinutes);
        PORTINFORMATIONLatitudeDecimal=findViewById(R.id.portinformationLatitudeDecimal);
        PORTINFORMATIONLongitudeDegrees=findViewById(R.id.portinformationLongitudeDegrees);
        PORTINFORMATIONLongitudeMinutes=findViewById(R.id.portinformationLongitudeMinutes);
        PORTINFORMATIONLongitudeDecimal=findViewById(R.id.portinformationLongitudeDecimal);
        PORTINFORMATIONSupportingInformation=findViewById(R.id.portinformationSupportingInformation);
        PORTINFORMATIONspinner_latitude=findViewById(R.id.portinformationspinner_latitude);
        PORTINFORMATIONspinner_longtitude=findViewById(R.id.portinformationspinner_longitude);

        List<portinformationDraft> portDrafts=new ArrayList<>();

        portDrafts.addAll(PORTINFORMATIONdatabase.getportinformationDraft().getportinformationDraft());


        userName = PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getUserName();
        userEmail=   PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getEmail();
        userCompany = PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getCompany();
        userOccupation = PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getOccupation();
        userHomeCountry=  PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getHomeCountry();
        userVesselName=PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getVesselName();
        userIMONumber= PORTINFORMATIONdatabase.getDetails().getDetails().get(0).getImoNumber();

        PORTINFORMATIONSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkChartInput();
            }
        });

        PORTINFORMATIONCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askCameraPermissions();

            }
        });


        PORTINFORMATIONGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST_CODE);
            }
        });


        PORTINFORMATIONlocationPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(PortInformation.this, LocationPickerActivity.class);
                startActivityForResult(i, ADDRESS_PICKER_REQUEST);
            }
        });


        ArrayAdapter<CharSequence> latitude = ArrayAdapter.createFromResource(this,R.array.NS,android.R.layout.simple_spinner_item);

        latitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        PORTINFORMATIONspinner_latitude.setAdapter(latitude);
        PORTINFORMATIONspinner_latitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        portinformationlatitudepostion="N";
                        break;
                    case 1:
                        portinformationlatitudepostion="S";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        ArrayAdapter<CharSequence> longitude = ArrayAdapter.createFromResource(this,R.array.EW,android.R.layout.simple_spinner_item);

        longitude.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //  spinner_latitude.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        PORTINFORMATIONspinner_longtitude.setAdapter(longitude);
        PORTINFORMATIONspinner_longtitude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        portinformationlongitudeposition="E";
                        break;
                    case 1:
                        portinformationlongitudeposition="W";
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Intent intent = getIntent();

        String publicationdraft="NO";
        int selector=intent.getIntExtra("PORTINFORMATIONDraft",0);

        //Toast.makeText(this, chartdraft+"", Toast.LENGTH_SHORT).show();
        if(selector==1)
        {

            if(portDrafts.size()>0)
            {


            //Toast.makeText(this, "DONEEE"+"sdadasd"+database.getChart().getChartDraft().get(0).getLatitudeDecimal(), Toast.LENGTH_SHORT).show();
            PORTINFORMATIONNameOfPort.getEditText().setText(portDrafts.get(0).getNameOfPort());
            PORTINFORMATIONCountry.getEditText().setText(portDrafts.get(0).getCountry());
            PORTINFORMATIONAnchorages.getEditText().setText(portDrafts.get(0).getAnchorages());
            PORTINFORMATIONPilotage.getEditText().setText(portDrafts.get(0).getPilotage());
            PORTINFORMATIONDirections.getEditText().setText(portDrafts.get(0).getDirections());
            PORTINFORMATIONTug.getEditText().setText(portDrafts.get(0).getTug());
            PORTINFORMATIONWharves.getEditText().setText(portDrafts.get(0).getWharves());
            PORTINFORMATIONCargoHandling.getEditText().setText(portDrafts.get(0).getCargoHandling());
            PORTINFORMATIONRepairAndRescue.getEditText().setText(portDrafts.get(0).getRepairAndRescue());
            PORTINFORMATIONSupply.getEditText().setText(portDrafts.get(0).getSupply());
            PORTINFORMATIONServices.getEditText().setText(portDrafts.get(0).getServices());
            PORTINFORMATIONCommunication.getEditText().setText(portDrafts.get(0).getCommunications());
            PORTINFORMATIONPortAuthority.getEditText().setText(portDrafts.get(0).getPortAuthority());
            PORTINFORMATIONView.getEditText().setText(portDrafts.get(0).getView());
            PORTINFORMATIONLatitudeDegree.getEditText().setText(portDrafts.get(0).getLatitudeDegree());
            PORTINFORMATIONLatitudeMinutes.getEditText().setText(portDrafts.get(0).getLatitudeMinutes());
            PORTINFORMATIONLatitudeDecimal.getEditText().setText(portDrafts.get(0).getLatitudeDecimal());
            String latitudeP=portDrafts.get(0).getLatitudePosition();

          //  Toast.makeText(this, latitudeP+"", Toast.LENGTH_SHORT).show();
            if(latitudeP.equals("N"))
            {
                //  Toast.makeText(this, "N", Toast.LENGTH_SHORT).show();
                PORTINFORMATIONspinner_latitude.setSelection(0);
            }
            if(latitudeP.equals("S")){
                //  Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                PORTINFORMATIONspinner_latitude.setSelection(1);
            }
            PORTINFORMATIONLongitudeDegrees.getEditText().setText(portDrafts.get(0).getLongitudeDegree());
            PORTINFORMATIONLongitudeMinutes.getEditText().setText(portDrafts.get(0).getLongitudeMinutes());
            PORTINFORMATIONLongitudeDecimal.getEditText().setText(portDrafts.get(0).getLongitudeDecimal());
            String longitudeP=portDrafts.get(0).getLongitudePosition();
         //   Toast.makeText(this, longitudeP+"", Toast.LENGTH_SHORT).show();

            if(longitudeP.equals("E"))
            {
                //     Toast.makeText(this, "E", Toast.LENGTH_SHORT).show();
                PORTINFORMATIONspinner_longtitude.setSelection(0);
            }
            if(longitudeP.equals("W")){
                //    Toast.makeText(this, "W", Toast.LENGTH_SHORT).show();
                PORTINFORMATIONspinner_longtitude.setSelection(1);
            }


            PORTINFORMATIONSupportingInformation.getEditText().setText(portDrafts.get(0).getObservation());
            for(int i=0;i<portDrafts.get(0).getURLIMAGE().size();i++)
            {

                if(!portDrafts.get(0).getURLIMAGE().get(i).contains("content:"))
                {
                    PORTINFORMATIONfileURI.add(portDrafts.get(0).getURLIMAGE().get(i));
                }
                Log.e("img",portDrafts.get(0).getURLIMAGE().get(i));

            }
            PORTINFORMATIONrecycler.notifyDataSetChanged();

        }

        }
    }


    private void checkChartInput() {

        if(!validatePORTINFORMATIONNumberOfPort()|!validatePORTINFORMATIONCountry()|!validatePORTINFORMATIONLatitudeDegree()|!validatePORTINFORMATIONLatitudeMinutes()|!validatePORTINFORMATIONLatitudeDecimal()|!validatePORTINFORMATIONLongitudeDegree()|!validatePORTINFORMATIONLongitudeMinutes()|!validatePORTINFORMATIONLongitudeDecimal())
        {
            return;
        }

        portinformationsupportinginformation = PORTINFORMATIONSupportingInformation.getEditText().getText().toString().trim();

        portinformationanchorages= PORTINFORMATIONAnchorages.getEditText().getText().toString().trim();
        portinformationpilotage= PORTINFORMATIONPilotage.getEditText().getText().toString().trim();
        portinformationdirections= PORTINFORMATIONDirections.getEditText().getText().toString().trim();
        portinformationtug= PORTINFORMATIONTug.getEditText().getText().toString().trim();
        portinformationwharves= PORTINFORMATIONWharves.getEditText().getText().toString().trim();
        portinformationcargohandling= PORTINFORMATIONCargoHandling.getEditText().getText().toString().trim();
        portinformationrepairandrescue= PORTINFORMATIONRepairAndRescue.getEditText().getText().toString().trim();
        portinformationsupply= PORTINFORMATIONSupply.getEditText().getText().toString().trim();
        portinformationservices= PORTINFORMATIONServices.getEditText().getText().toString().trim();
        portinformationcommunication= PORTINFORMATIONCommunication.getEditText().getText().toString().trim();
        portinformationportauthority= PORTINFORMATIONPortAuthority.getEditText().getText().toString().trim();
        portinformationview= PORTINFORMATIONView.getEditText().getText().toString().trim();


      //  Toast.makeText(this, "DONE", Toast.LENGTH_SHORT).show();
        Progress.setTitle("Uploading Information..");
        Progress.setMessage("PLEASE WAIT WE ARE UPLOADING INFORMATION..");
        Progress.setCanceledOnTouchOutside(false);
        Progress.show();
        SaveData();
    }

    private void SaveData() {
        PORTINFORMATIONdownloadFilePath.clear();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat currentDate = new SimpleDateFormat("MMM dd, yyyy");
        saveCurrentDate = currentDate.format(calendar.getTime());

        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss a"); // it will give us the currentTime a means am and pm
        saveCurrentTime = currentTime.format(calendar.getTime());
        RandomKEY = saveCurrentDate + saveCurrentTime;
        if (PORTINFORMATIONfileURI.size() > 0) {
            for (int i = 0; i < PORTINFORMATIONfileURI.size(); i++) {
                final StorageReference filePath;
                filePath =  portinformationImageRef.child(Uri.parse(PORTINFORMATIONfileURI.get(i)).getLastPathSegment() + RandomKEY + ".jpg"); //last path segment is used to get the imageuri last / part which is basically image name in the uri
                final UploadTask uploadTask = filePath.putFile(Uri.parse(PORTINFORMATIONfileURI.get(i)));
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        String messege = e.toString();
                        Progress.dismiss();
                        Toast.makeText(PortInformation.this, "Error: " + messege + "..", Toast.LENGTH_SHORT).show();

                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() { // tasksnapshot show about the state of the running upload task
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        //       Toast.makeText(ChartActivity.this, "Image Stored Successfully..", Toast.LENGTH_SHORT).show();

                        Task<Uri> taskUrl = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                            @Override
                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                if (!task.isSuccessful()) {

                                    throw task.getException();
                                }
                                downloadPath = filePath.getDownloadUrl().toString();

                                return filePath.getDownloadUrl();
                            }
                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {

                                    downloadPath = task.getResult().toString();
                                    Log.e("image", downloadPath);
                                    //Toast.makeText(ChartActivity.this, downloadPath + "", Toast.LENGTH_SHORT).show();
                                    PORTINFORMATIONdownloadFilePath.add(downloadPath);
                                    // Toast.makeText(ChartActivity.this, "Product Image URL Got Successfully..", Toast.LENGTH_SHORT).show();
                                    // SaveProductInfoToDatabase();
                                    Log.e("TAGINGINGIG",PORTINFORMATIONdownloadFilePath.size()+"  "+ PORTINFORMATIONfileURI.size());
                                    if (PORTINFORMATIONdownloadFilePath.size() == PORTINFORMATIONfileURI.size()) {
                                        SaveProductInfoToPORTINFORMATION();
                                    }
                                }
                            }
                        });
                    }
                });

            }
            Log.e("calling", "work");

            //    SaveProductInfoToChart();
        }

        else
        {

            HashMap<String ,Object> hashMap= new HashMap<>();
            // hashMap.put("image",downloadFilePath.get(0));
            hashMap.put("UserName",userName);
            hashMap.put("UserEmail",userEmail);
            hashMap.put("UserCompany",userCompany);
            hashMap.put("UserOccupation",userOccupation);
            hashMap.put("UserHomeCountry",userHomeCountry);
            hashMap.put("UserVesselName",userVesselName);
            hashMap.put("UserIMONumber",userIMONumber);
            hashMap.put("PORTINFORMATIONNameOfPort",portinformationnameofport);
            hashMap.put("PORTINFORMATIONCountry",portinformationcountry);
            hashMap.put("PORTINFORMATIONLatitudeDegree",portinformationlatitudedegree);
            hashMap.put("PORTINFORMATIONLatitudeMinutes",portinformationlatitudeminutes);
            hashMap.put("PORTINFORMATIONLatitudeDecimals",portinformationlatitudedecimal);
            hashMap.put("PORTINFORMATIONLatitudePosition",portinformationlatitudepostion);
            hashMap.put("PORTINFORMATIONLongitudeDegree",portinformationlongitudedegrees);
            hashMap.put("PORTINFORMATIONLongitudeMinutes",portinformationlongtitugeminutes);
            hashMap.put("PORTINFORMATIONLongitudeDecimals",portinformationlongitudedecimal);
            hashMap.put("PORTINFORMATIONLongitudePosition",portinformationlongitudeposition);
            hashMap.put("PORTINFORMATIONSupportingInformation",portinformationsupportinginformation);
            hashMap.put("PORTINFORMATIONAnchorages",portinformationanchorages);
            hashMap.put("PORTINFORMATIONPilotage",portinformationpilotage);
            hashMap.put("PORTINFORMATIONDirections",portinformationdirections);
            hashMap.put("PORTINFORMATIONTug",portinformationtug);
            hashMap.put("PORTINFORMATIONWharves",portinformationwharves);
            hashMap.put("PORTINFORMATIONCargoHandling",portinformationcargohandling);
            hashMap.put("PORTINFORMATIONRepairAndRescue",portinformationrepairandrescue);
            hashMap.put("PORTINFORMATIONSupply",portinformationsupply);
            hashMap.put("PORTINFORMATIONServices",portinformationservices);
            hashMap.put("PORTINFORMATIONCommunication",portinformationcommunication);
            hashMap.put("PORTINFORMATIONPortAuthority",portinformationportauthority);
            hashMap.put("PORTINFORMATIONView",portinformationview);;
            hashMap.put("PORTINFORMATIONSupportingInformation",portinformationsupportinginformation);
            hashMap.put("date",saveCurrentDate);
            hashMap.put("time",saveCurrentTime);






            portinformationdataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful())
                    {

                        Toast.makeText(PortInformation.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                        Progress.dismiss();
                    }
                    else {
                        //   loginProgress.dismiss();
                        String messege =task.getException().toString();
                        Progress.dismiss();
                        Toast.makeText(PortInformation.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


    }

    private void SaveProductInfoToPORTINFORMATION() {
        HashMap<String ,Object> hashMap= new HashMap<>();
        // hashMap.put("image",downloadFilePath.get(0));
        for(int i=0;i<PORTINFORMATIONdownloadFilePath.size();i++)
        {
            hashMap.put("image"+i,PORTINFORMATIONdownloadFilePath.get(i));
        }
        hashMap.put("UserName",userName);
        hashMap.put("UserEmail",userEmail);
        hashMap.put("UserCompany",userCompany);
        hashMap.put("UserOccupation",userOccupation);
        hashMap.put("UserHomeCountry",userHomeCountry);
        hashMap.put("UserVesselName",userVesselName);
        hashMap.put("UserIMONumber",userIMONumber);
        hashMap.put("PORTINFORMATIONNameOfPort",portinformationnameofport);
        hashMap.put("PORTINFORMATIONCountry",portinformationcountry);
        hashMap.put("PORTINFORMATIONLatitudeDegree",portinformationlatitudedegree);
        hashMap.put("PORTINFORMATIONLatitudeMinutes",portinformationlatitudeminutes);
        hashMap.put("PORTINFORMATIONLatitudeDecimals",portinformationlatitudedecimal);
        hashMap.put("PORTINFORMATIONLatitudePosition",portinformationlatitudepostion);
        hashMap.put("PORTINFORMATIONLongitudeDegree",portinformationlongitudedegrees);
        hashMap.put("PORTINFORMATIONLongitudeMinutes",portinformationlongtitugeminutes);
        hashMap.put("PORTINFORMATIONLongitudeDecimals",portinformationlongitudedecimal);
        hashMap.put("PORTINFORMATIONLongitudePosition",portinformationlongitudeposition);
        hashMap.put("PORTINFORMATIONSupportingInformation",portinformationsupportinginformation);
        hashMap.put("PORTINFORMATIONAnchorages",portinformationanchorages);
        hashMap.put("PORTINFORMATIONPilotage",portinformationpilotage);
        hashMap.put("PORTINFORMATIONDirections",portinformationdirections);
        hashMap.put("PORTINFORMATIONTug",portinformationtug);
        hashMap.put("PORTINFORMATIONWharves",portinformationwharves);
        hashMap.put("PORTINFORMATIONCargoHandling",portinformationcargohandling);
        hashMap.put("PORTINFORMATIONRepairAndRescue",portinformationrepairandrescue);
        hashMap.put("PORTINFORMATIONSupply",portinformationsupply);
        hashMap.put("PORTINFORMATIONServices",portinformationservices);
        hashMap.put("PORTINFORMATIONCommunication",portinformationcommunication);
        hashMap.put("PORTINFORMATIONPortAuthority",portinformationportauthority);
        hashMap.put("PORTINFORMATIONView",portinformationview);;
        hashMap.put("PORTINFORMATIONSupportingInformation",portinformationsupportinginformation);
        hashMap.put("date",saveCurrentDate);
        hashMap.put("time",saveCurrentTime);










        portinformationdataRef.child(RandomKEY).updateChildren(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    Progress.dismiss();
                    Toast.makeText(PortInformation.this,"Details Submitted Successfully",Toast.LENGTH_SHORT).show();
                }
                else {
                    //   loginProgress.dismiss();
                    Progress.dismiss();
                    String messege =task.getException().toString();
                    Toast.makeText(PortInformation.this,"Error: " +messege+"..",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERM_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                dispatchTakePictureIntent();
            } else {
                Toast.makeText(this, "Camera Permission is Required to Use camera.", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onBackPressed() {

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        PORTINFORMATIONdatabase = Room.databaseBuilder(PortInformation.this, DetailsDatabase.class, "DETAILSDATABASE").allowMainThreadQueries().build();

                        List<portinformationDraft> portinfoDrafts = new ArrayList<>();

                        portinfoDrafts.addAll(PORTINFORMATIONdatabase.getportinformationDraft().getportinformationDraft());

                        Log.e("SIZEEEEE",portinfoDrafts.size()+"");

                        if(portinfoDrafts.size()>0) {

                         //   Toast.makeText(PortInformation.this, "UPDATE", Toast.LENGTH_SHORT).show();
                      //    Log.e("UPDATE","DONE");
                            portinformationDraft portinfoDraft = new portinformationDraft(0,PORTINFORMATIONNameOfPort.getEditText().getText().toString(),PORTINFORMATIONCountry.getEditText().getText().toString(),PORTINFORMATIONLatitudeDegree.getEditText().getText().toString(),PORTINFORMATIONLatitudeMinutes.getEditText().getText().toString(),PORTINFORMATIONLatitudeDecimal.getEditText().getText().toString(),portinformationlatitudepostion,PORTINFORMATIONLongitudeDegrees.getEditText().getText().toString(),PORTINFORMATIONLongitudeMinutes.getEditText().getText().toString(),PORTINFORMATIONLongitudeDecimal.getEditText().getText().toString(),portinformationlongitudeposition,PORTINFORMATIONAnchorages.getEditText().getText().toString(),PORTINFORMATIONPilotage.getEditText().getText().toString(),PORTINFORMATIONDirections.getEditText().getText().toString(),PORTINFORMATIONTug.getEditText().getText().toString(),PORTINFORMATIONWharves.getEditText().getText().toString(),PORTINFORMATIONCargoHandling.getEditText().getText().toString(),PORTINFORMATIONRepairAndRescue.getEditText().getText().toString(),PORTINFORMATIONSupply.getEditText().getText().toString(),PORTINFORMATIONServices.getEditText().getText().toString(),PORTINFORMATIONCommunication.getEditText().getText().toString(),PORTINFORMATIONPortAuthority.getEditText().getText().toString(),PORTINFORMATIONView.getEditText().getText().toString(),PORTINFORMATIONSupportingInformation.getEditText().getText().toString(),PORTINFORMATIONfileURI);

                            PORTINFORMATIONdatabase.getportinformationDraft().updateportinformationDraft(portinfoDraft);
                        }
                        else
                        {
                            //   Log.e("FIRSTTIME","DONEEE");

                            portinformationDraft portinfoDraft = new portinformationDraft(0,PORTINFORMATIONNameOfPort.getEditText().getText().toString(),PORTINFORMATIONCountry.getEditText().getText().toString(),PORTINFORMATIONLatitudeDegree.getEditText().getText().toString(),PORTINFORMATIONLatitudeMinutes.getEditText().getText().toString(),PORTINFORMATIONLatitudeDecimal.getEditText().getText().toString(),portinformationlatitudepostion,PORTINFORMATIONLongitudeDegrees.getEditText().getText().toString(),PORTINFORMATIONLongitudeMinutes.getEditText().getText().toString(),PORTINFORMATIONLongitudeDecimal.getEditText().getText().toString(),portinformationlongitudeposition,PORTINFORMATIONAnchorages.getEditText().getText().toString(),PORTINFORMATIONPilotage.getEditText().getText().toString(),PORTINFORMATIONDirections.getEditText().getText().toString(),PORTINFORMATIONTug.getEditText().getText().toString(),PORTINFORMATIONWharves.getEditText().getText().toString(),PORTINFORMATIONCargoHandling.getEditText().getText().toString(),PORTINFORMATIONRepairAndRescue.getEditText().getText().toString(),PORTINFORMATIONSupply.getEditText().getText().toString(),PORTINFORMATIONServices.getEditText().getText().toString(),PORTINFORMATIONCommunication.getEditText().getText().toString(),PORTINFORMATIONPortAuthority.getEditText().getText().toString(),PORTINFORMATIONView.getEditText().getText().toString(),PORTINFORMATIONSupportingInformation.getEditText().getText().toString(),PORTINFORMATIONfileURI);
                            PORTINFORMATIONdatabase.getportinformationDraft().addportInformationDraft(portinfoDraft);
                        }



                        finish();
                        //Yes button clicked
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked


                        finish();
                        break;
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(PortInformation.this);
        builder.setMessage("Would You Like To Save Draft?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                File f = new File(PORTINFORMATIONcurrentPhotoPath);
                //selectedImage.setImageURI(Uri.fromFile(f));
                Log.d("tag", "ABsolute Url of Image is " + Uri.fromFile(f));
                //  f.getAbsolutePath();
                Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri contentUri = Uri.fromFile(f);
                Log.e("api", contentUri.toString());

                mediaScanIntent.setData(contentUri);
                this.sendBroadcast(mediaScanIntent);
                PORTINFORMATIONfileURI.add(contentUri.toString());
                PORTINFORMATIONrecycler.notifyDataSetChanged();
            }

        }

//        if (requestCode == GALLERY_REQUEST_CODE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri contentUri = data.getData();
//                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
//                String imageFileName = "JPEG_" + timeStamp + "." + getFileExt(contentUri);
//                Log.d("tag", "onActivityResult: Gallery Image Uri:  " + imageFileName);
//                selectedImage.setImageURI(contentUri);
//            }
//
//        }
        if (requestCode == GALLERY_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data.getClipData() != null) {

                int totalItemsSelected = data.getClipData().getItemCount();

                for (int i = 0; i < totalItemsSelected; i++) {

                    Uri fileUri = data.getClipData().getItemAt(i).getUri();

                    String fileName = getFileName(fileUri);

                    PORTINFORMATIONfileURI.add(fileUri.toString());
                    PORTINFORMATIONrecycler.notifyDataSetChanged();
                    Log.e("tagg",PORTINFORMATIONfileURI.get(i));
//                    fileNameList.add(fileName);
//                    fileDoneList.add("uploading");
//                    uploadListAdapter.notifyDataSetChanged();

//                 StorageReference fileToUpload = mStorage.child("Images").child(fileName);
////
//                    final int finalI = i;
//                    fileToUpload.putFile(fileUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//                        @Override
//                       public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
////                            fileDoneList.remove(finalI);
////                            fileDoneList.add(finalI, "done");
////
////                            uploadListAdapter.notifyDataSetChanged();
//                            Toast.makeText(ChartActivity.this, "done", Toast.LENGTH_SHORT).show();
////
//                      }
//                    });

                }

                //Toast.makeText(MainActivity.this, "Selected Multiple Files", Toast.LENGTH_SHORT).show();

            } else if (data.getData() != null) {

                Uri im = data.getData();
                PORTINFORMATIONfileURI.add(im.toString());
                PORTINFORMATIONrecycler.notifyDataSetChanged();
              //  Toast.makeText(PortInformation.this, "Selected Single File", Toast.LENGTH_SHORT).show();

            }

        }
        if (requestCode == ADDRESS_PICKER_REQUEST) {
            try {
                if (data != null && data.getStringExtra(MapUtility.ADDRESS) != null) {
                    String address = data.getStringExtra(MapUtility.ADDRESS);
                    double selectedLatitude = data.getDoubleExtra(MapUtility.LATITUDE, 0.0);
                    double selectedLongitude = data.getDoubleExtra(MapUtility.LONGITUDE, 0.0);
                    //    txtAddress.setText("Address: "+address);
                    //    txtLatLong.setText("Lat:"+selectedLatitude+"  Long:"+selectedLongitude);
                    // Toast.makeText(this, ""+selectedLatitude+""+selectedLongitude, Toast.LENGTH_SHORT).show();
                    getFormattedLocationInDegree(selectedLatitude, selectedLongitude);
                    Log.e("taging",selectedLatitude+""+selectedLongitude);

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }


    private void askCameraPermissions() {


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERM_CODE);
        } else {
            dispatchTakePictureIntent();
        }

    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        //File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File storageDir = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        PORTINFORMATIONcurrentPhotoPath = image.getAbsolutePath();
        Log.e("current", PORTINFORMATIONcurrentPhotoPath);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
      //  Toast.makeText(this, "asd", Toast.LENGTH_SHORT).show();
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
       //     Toast.makeText(this, "asdasdasddasdasd", Toast.LENGTH_SHORT).show();
            File photoFile = null;
            try {
                photoFile = createImageFile();

            } catch (IOException ex) {

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.hydrochart.hydrochart.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, CAMERA_REQUEST_CODE);
            }
        }
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public  String getFormattedLocationInDegree(double latitude, double longitude) {
        try {

            int latSeconds = (int) Math.round(latitude * 3600);
            int latDegrees = latSeconds / 3600;
            latSeconds = Math.abs(latSeconds % 3600);
            int latMinutes = latSeconds / 60;
            latSeconds %= 60;

            int longSeconds = (int) Math.round(longitude * 3600);
            int longDegrees = longSeconds / 3600;
            longSeconds = Math.abs(longSeconds % 3600);
            int longMinutes = longSeconds / 60;
            longSeconds %= 60;
            String latDegree = latDegrees >= 0 ? "N" : "S";
            String lonDegrees = longDegrees >= 0 ? "E" : "W";



            PORTINFORMATIONLatitudeDegree.getEditText().setText(Math.abs(latDegrees)+"");
            PORTINFORMATIONLatitudeMinutes.getEditText().setText(latMinutes+"");
            PORTINFORMATIONLatitudeDecimal.getEditText().setText(latSeconds+"");

            PORTINFORMATIONLongitudeDegrees.getEditText().setText(Math.abs(longDegrees)+"");
            PORTINFORMATIONLongitudeMinutes.getEditText().setText(longMinutes+"");
            PORTINFORMATIONLongitudeDecimal.getEditText().setText(longSeconds+"");

            if(latDegree.equals("N"))
            {
            //    Toast.makeText(this, "GOOD", Toast.LENGTH_SHORT).show();
                PORTINFORMATIONspinner_latitude.setSelection(0);
            }
            if(latDegree.equals("S"))
            {
          //      Toast.makeText(this, "S", Toast.LENGTH_SHORT).show();
                PORTINFORMATIONspinner_latitude.setSelection(1);
            }
            if(lonDegrees.equals("E"))
            {
                PORTINFORMATIONspinner_longtitude.setSelection(0);
            }

            if(lonDegrees.equals("W"))
            {
                PORTINFORMATIONspinner_longtitude.setSelection(1);
            }


            Log.e("tagssss",Math.abs(latDegrees)+""+latMinutes+""+latSeconds+""+Math.abs(longDegrees)+""+longMinutes+""+longSeconds+"");

            return Math.abs(latDegrees) + "°" + latMinutes + "'" + latSeconds
                    + "\"" + latDegree + " " + Math.abs(longDegrees) + "°" + longMinutes
                    + "'" + longSeconds + "\"" + lonDegrees;
        } catch (Exception e) {
            return "" + String.format("%8.5f", latitude) + "  "
                    + String.format("%8.5f", longitude);
        }
    }





    private boolean validatePORTINFORMATIONNumberOfPort() {
        portinformationnameofport = PORTINFORMATIONNameOfPort.getEditText().getText().toString().trim();

        if (portinformationnameofport.isEmpty()) {
            PORTINFORMATIONNameOfPort.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONNameOfPort.setError(null);
            return true;
        }
    }

    private boolean validatePORTINFORMATIONCountry() {
        portinformationcountry = PORTINFORMATIONCountry.getEditText().getText().toString().trim();

        if (portinformationcountry.isEmpty()) {
            PORTINFORMATIONCountry.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONCountry.setError(null);
            return true;
        }
    }




    private boolean validatePORTINFORMATIONLatitudeDegree() {
        portinformationlatitudedegree = PORTINFORMATIONLatitudeDegree.getEditText().getText().toString().trim();
        if (portinformationlatitudedegree.isEmpty()) {
            PORTINFORMATIONLatitudeDegree.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONLatitudeDegree.setError(null);
            return true;
        }
    }

    private boolean validatePORTINFORMATIONLatitudeMinutes() {
        portinformationlatitudeminutes = PORTINFORMATIONLatitudeMinutes.getEditText().getText().toString().trim();
        if (portinformationlatitudeminutes.isEmpty()) {
            PORTINFORMATIONLatitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONLatitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validatePORTINFORMATIONLatitudeDecimal() {
        portinformationlatitudedecimal =  PORTINFORMATIONLatitudeDecimal.getEditText().getText().toString().trim();
        if (portinformationlatitudedecimal.isEmpty()) {
            PORTINFORMATIONLatitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONLatitudeDecimal.setError(null);
            return true;
        }
    }

    private boolean validatePORTINFORMATIONLongitudeDegree() {
        portinformationlongitudedegrees = PORTINFORMATIONLongitudeDegrees.getEditText().getText().toString().trim();
        if (portinformationlongitudedegrees.isEmpty()) {
            PORTINFORMATIONLongitudeDegrees.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONLongitudeDegrees.setError(null);
            return true;
        }
    }

    private boolean validatePORTINFORMATIONLongitudeMinutes() {
        portinformationlongtitugeminutes =   PORTINFORMATIONLongitudeMinutes.getEditText().getText().toString().trim();
        if (portinformationlongtitugeminutes.isEmpty()) {
            PORTINFORMATIONLongitudeMinutes.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONLongitudeMinutes.setError(null);
            return true;
        }
    }

    private boolean validatePORTINFORMATIONLongitudeDecimal() {
        portinformationlongitudedecimal = PORTINFORMATIONLongitudeDecimal.getEditText().getText().toString().trim();
        if (portinformationlongitudedecimal.isEmpty()) {
            PORTINFORMATIONLongitudeDecimal.setError("Field can't be empty");
            return false;
        }

        else  {

            PORTINFORMATIONLongitudeDecimal.setError(null);
            return true;
        }
    }

}
