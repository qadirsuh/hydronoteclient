package com.hydrochart.hydrochart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

public class HomeActivity extends AppCompatActivity {
    Toolbar toolbar;
    CardView chartBtn,encBtn,publicationBtn,portinformationBtn,marinelifeBtn,managenotesBtn;
    ImageView settingHome;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        chartBtn =findViewById(R.id.chartBtn);
        settingHome=findViewById(R.id.settingHome);
        fab=findViewById(R.id.fab);
        encBtn=findViewById(R.id.encBtn);
        portinformationBtn=findViewById(R.id.portinformationBtn);
        publicationBtn=findViewById(R.id.publicationBtn);
        marinelifeBtn=findViewById(R.id.marinelifeBtn);
        managenotesBtn=findViewById(R.id.manageNoteBtn);
        chartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,ChartActivity.class);
                startActivity(intent);
            }
        });

        encBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,ENCActivity.class);
                startActivity(intent);
            }
        });

        publicationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,PublicationActivity.class);
                startActivity(intent);
            }
        });

        portinformationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,PortInformation.class);
                startActivity(intent);
            }
        });

        marinelifeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,MarineLife.class);
                startActivity(intent);
            }
        });

        managenotesBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,ManageActivity.class);
                startActivity(intent);
            }
        });

        settingHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this,SettingActivity.class);
                startActivity(intent);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mIntent = new Intent(HomeActivity.this, ChatWithAdminActivity.class);
                mIntent.putExtra("PREVIOUS_ACTIVITY", HomeActivity.class.getSimpleName());
                startActivity(mIntent);

            }
        });
    }
}
