package com.hydrochart.hydrochart;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class ManageActivity extends AppCompatActivity {
    Toolbar toolbar;
    CardView managechartBtn,manageencBtn,managepublicationBtn,manageportinformationBtn,managemarinelifeBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        managechartBtn =findViewById(R.id.managechartBtn);
        manageencBtn=findViewById(R.id.manageencBtn);
        manageportinformationBtn=findViewById(R.id.manageportinformationBtn);
        managepublicationBtn=findViewById(R.id.managepublicationBtn);
        managemarinelifeBtn=findViewById(R.id.managemarinelifeBtn);


        managechartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this,ChartActivity.class);
                intent.putExtra("ChartDraft",1);
                startActivity(intent);
            }
        });

        manageencBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this,ENCActivity.class);
                intent.putExtra("ENCDraft",1);
                startActivity(intent);
            }
        });

        managepublicationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this,PublicationActivity.class);
                intent.putExtra("PUBLICATIONDraft",1);
                startActivity(intent);
            }
        });

        manageportinformationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this,PortInformation.class);
                intent.putExtra("PORTINFORMATIONDraft",1);
                startActivity(intent);
            }
        });

        managemarinelifeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManageActivity.this,MarineLife.class);
                intent.putExtra("MARINELIFEDraft",1);
                startActivity(intent);
            }
        });

    }
}
