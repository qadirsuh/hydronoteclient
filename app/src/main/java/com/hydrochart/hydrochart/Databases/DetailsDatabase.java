package com.hydrochart.hydrochart.Databases;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.hydrochart.hydrochart.PortInformation;

@TypeConverters(value = GithubTypeConverters.class)
@Database(entities = {DetailsEntity.class,ChartDraft.class,ENCDraft.class,PublicationDraft.class,portinformationDraft.class,MarineLifeDraft.class},version = 1,exportSchema = false)
public abstract class DetailsDatabase extends RoomDatabase {

    abstract public DetailsDAO getDetails();

    abstract public ChartDAO getChart();

    abstract public ENCDAO getENCDraft();

    abstract public PublicationDAO getPublicationDraft();

    abstract public portInformationDAO getportinformationDraft();

    abstract public MarineLifeDAO getMarineLifeDraft();
}
