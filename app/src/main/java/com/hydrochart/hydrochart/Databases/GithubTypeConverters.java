package com.hydrochart.hydrochart.Databases;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

class GithubTypeConverters {
    static Gson gson = new Gson();

    @TypeConverter
    public static List<String> stringToURLIMAGEList(String data) {
        if (data == null) {
            return Collections.emptyList();
        }

        Type listType = new TypeToken<List<String>>() {}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static String URLIMAGEListToString(List<String> someObjects) {
        return gson.toJson(someObjects);
    }
}
