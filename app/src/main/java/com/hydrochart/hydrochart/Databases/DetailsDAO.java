package com.hydrochart.hydrochart.Databases;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DetailsDAO {

    @Insert long addDetails(DetailsEntity detailsEntity);

    @Update void updateDetails(DetailsEntity detailsEntity);

    @Query("select * from Details")
    List<DetailsEntity> getDetails();

    @Query("select * from Details where primaryid==:id")
    List<DetailsEntity> getDetailsById(long id);

}
