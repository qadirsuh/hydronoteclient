package com.hydrochart.hydrochart.Databases;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Details")
public class DetailsEntity {

    @PrimaryKey(autoGenerate =false)
    int primaryid;

    @ColumnInfo(name = "userName")
    String userName;

    @ColumnInfo(name = "Email")
    String Email;

    @ColumnInfo(name = "Company")
    String Company;

    @ColumnInfo(name = "vesselName")
    String vesselName;

    @ColumnInfo(name = "imoNumber")
    String imoNumber;

    @ColumnInfo(name = "occupation")
    String occupation;

    @ColumnInfo(name = "homeCountry")
    String homeCountry;

    @ColumnInfo(name = "exist")
    Boolean exist;

    public DetailsEntity(int primaryid, String userName, String Email, String Company, String vesselName, String imoNumber, String occupation, String homeCountry, Boolean exist) {
        this.primaryid = primaryid;
        this.userName = userName;
        this.Email = Email;
        this.Company = Company;
        this.vesselName = vesselName;
        this.imoNumber = imoNumber;
        this.occupation = occupation;
        this.homeCountry = homeCountry;
        this.exist=exist;
    }


    public Boolean getExist() {
        return exist;
    }

    public void setExist(Boolean exist) {
        this.exist = exist;
    }

    public int getPrimaryid() {
        return primaryid;
    }

    public void setPrimaryid(int primaryid) {
        this.primaryid = primaryid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getImoNumber() {
        return imoNumber;
    }

    public void setImoNumber(String imoNumber) {
        this.imoNumber = imoNumber;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getHomeCountry() {
        return homeCountry;
    }

    public void setHomeCountry(String homeCountry) {
        this.homeCountry = homeCountry;
    }
}
