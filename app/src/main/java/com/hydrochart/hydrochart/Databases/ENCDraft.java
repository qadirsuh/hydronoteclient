package com.hydrochart.hydrochart.Databases;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity(tableName = "ENCDraft")
public class ENCDraft {


    @PrimaryKey(autoGenerate = false)
    int primaryid;


    @ColumnInfo(name = "ENCAffected")
    String ENCAffected;

    @ColumnInfo(name = "NMWeek")
    String NMWeek;

    @ColumnInfo(name = "NMYear")
    String NMYear;

    @ColumnInfo(name = "LatitudeDegree")
    String LatitudeDegree;

    @ColumnInfo(name = "LatitudeMinutes")
    String LatitudeMinutes;

    @ColumnInfo(name = "LatitudeDecimal")
    String LatitudeDecimal;

    @ColumnInfo(name = "LatitudePosition")
    String LatitudePosition;

    @ColumnInfo(name = "LongitudeDegree")
    String LongitudeDegree;

    @ColumnInfo(name = "LongitudeMinutes")
    String LongitudeMinutes;

    @ColumnInfo(name = "LongitudeDecimal")
    String LongitudeDecimal;


    @ColumnInfo(name = "LongitudePosition")
    String LongitudePosition;

    @ColumnInfo(name = "Observation")
    String Observation;


    @TypeConverters(GithubTypeConverters.class)
    public final List<String> URLIMAGE;


    public ENCDraft(int primaryid, String ENCAffected, String NMWeek, String NMYear, String LatitudeDegree, String LatitudeMinutes, String LatitudeDecimal,String LatitudePosition, String LongitudeDegree, String LongitudeMinutes, String LongitudeDecimal,String LongitudePosition, String Observation, List<String> URLIMAGE) {
        this.primaryid = primaryid;
        this.ENCAffected = ENCAffected;
        this.NMWeek = NMWeek;
        this.NMYear = NMYear;
        this.LatitudeDegree = LatitudeDegree;
        this.LatitudeMinutes = LatitudeMinutes;
        this.LatitudeDecimal = LatitudeDecimal;
        this.LatitudePosition=LatitudePosition;
        this.LongitudeDegree = LongitudeDegree;
        this.LongitudeMinutes = LongitudeMinutes;
        this.LongitudeDecimal = LongitudeDecimal;
        this.LongitudePosition=LongitudePosition;
        this.Observation = Observation;
        this.URLIMAGE = URLIMAGE;
    }

    public String getLatitudePosition() {
        return LatitudePosition;
    }

    public void setLatitudePosition(String latitudePosition) {
        LatitudePosition = latitudePosition;
    }

    public String getLongitudePosition() {
        return LongitudePosition;
    }

    public void setLongitudePosition(String longitudePosition) {
        LongitudePosition = longitudePosition;
    }

    public int getPrimaryid() {
        return primaryid;
    }

    public void setPrimaryid(int primaryid) {
        this.primaryid = primaryid;
    }

    public String getENCAffected() {
        return ENCAffected;
    }

    public void setENCAffected(String ENCAffected) {
        this.ENCAffected = ENCAffected;
    }

    public String getNMWeek() {
        return NMWeek;
    }

    public void setNMWeek(String NMWeek) {
        this.NMWeek = NMWeek;
    }

    public String getNMYear() {
        return NMYear;
    }

    public void setNMYear(String NMYear) {
        this.NMYear = NMYear;
    }

    public String getLatitudeDegree() {
        return LatitudeDegree;
    }

    public void setLatitudeDegree(String latitudeDegree) {
        LatitudeDegree = latitudeDegree;
    }

    public String getLatitudeMinutes() {
        return LatitudeMinutes;
    }

    public void setLatitudeMinutes(String latitudeMinutes) {
        LatitudeMinutes = latitudeMinutes;
    }

    public String getLatitudeDecimal() {
        return LatitudeDecimal;
    }

    public void setLatitudeDecimal(String latitudeDecimal) {
        LatitudeDecimal = latitudeDecimal;
    }

    public String getLongitudeDegree() {
        return LongitudeDegree;
    }

    public void setLongitudeDegree(String longitudeDegree) {
        LongitudeDegree = longitudeDegree;
    }

    public String getLongitudeMinutes() {
        return LongitudeMinutes;
    }

    public void setLongitudeMinutes(String longitudeMinutes) {
        LongitudeMinutes = longitudeMinutes;
    }

    public String getLongitudeDecimal() {
        return LongitudeDecimal;
    }

    public void setLongitudeDecimal(String longitudeDecimal) {
        LongitudeDecimal = longitudeDecimal;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String observation) {
        Observation = observation;
    }

    public List<String> getURLIMAGE() {
        return URLIMAGE;
    }
}
