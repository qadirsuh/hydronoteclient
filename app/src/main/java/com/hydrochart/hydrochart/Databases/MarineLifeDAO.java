package com.hydrochart.hydrochart.Databases;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface MarineLifeDAO {


    @Insert
    long addMarineLifeDraft(MarineLifeDraft marineLifeDraft);

    @Update
    void updateMarineLifeDraft(MarineLifeDraft marineLifeDraft);

    @Query("select * from MarineLifeDraft")
    List<MarineLifeDraft> getMarineLifeDraft();

    @Query("select * from MarineLifeDraft where primaryid==:id")
    List<MarineLifeDraft> getMarineLifeDraftById(long id);
}
