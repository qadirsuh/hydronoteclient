package com.hydrochart.hydrochart.Databases;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PublicationDAO {

    @Insert
    long addPublication(PublicationDraft publicationDraft);

    @Update
    void updatePublication(PublicationDraft publicationDraft);

    @Query("select * from PublicationDraft")
    List<PublicationDraft> getPublicationDraft();

    @Query("select * from PublicationDraft where primaryid==:id")
    List<PublicationDraft> getPublicationDraftById(long id);
}
