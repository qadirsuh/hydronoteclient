package com.hydrochart.hydrochart.Databases;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity(tableName = "portinformationDraft")
public class portinformationDraft {


    @PrimaryKey(autoGenerate = false)
    int primaryid;


    @ColumnInfo(name = "NamrOfPort")
    String NameOfPort;


    @ColumnInfo(name = "Country")
    String Country;



    @ColumnInfo(name = "LatitudeDegree")
    String LatitudeDegree;

    @ColumnInfo(name = "LatitudeMinutes")
    String LatitudeMinutes;

    @ColumnInfo(name = "LatitudeDecimal")
    String LatitudeDecimal;

    @ColumnInfo(name = "LatitudePosition")
    String LatitudePosition;


    @ColumnInfo(name = "LongitudeDegree")
    String LongitudeDegree;

    @ColumnInfo(name = "LongitudeMinutes")
    String LongitudeMinutes;

    @ColumnInfo(name = "LongitudeDecimal")
    String LongitudeDecimal;

    @ColumnInfo(name = "LongitudePosition")
    String LongitudePosition;

    @ColumnInfo(name = "Anchorages")
    String Anchorages;

    @ColumnInfo(name = "Pilotage")
    String Pilotage;

    @ColumnInfo(name = "Directions")
    String Directions;

    @ColumnInfo(name = "Tug")
    String Tug;

    @ColumnInfo(name = "Wharves")
    String Wharves;

    @ColumnInfo(name = "CargoHandling")
    String CargoHandling;

    @ColumnInfo(name = "RepairAndRescue")
    String RepairAndRescue;

    @ColumnInfo(name = "Supply")
    String Supply;

    @ColumnInfo(name = "Services")
    String Services;

    @ColumnInfo(name = "Communications")
    String Communications;

    @ColumnInfo(name = "PortAuthority")
    String PortAuthority;

    @ColumnInfo(name = "View")
    String View;


    @ColumnInfo(name = "Observation")
    String Observation;

    @TypeConverters(GithubTypeConverters.class)
    public final List<String> URLIMAGE;



    public portinformationDraft(int primaryid, String NameOfPort, String Country, String LatitudeDegree, String LatitudeMinutes, String LatitudeDecimal, String LatitudePosition, String LongitudeDegree, String LongitudeMinutes, String LongitudeDecimal, String LongitudePosition, String Anchorages, String Pilotage, String Directions, String Tug, String Wharves, String CargoHandling, String RepairAndRescue, String Supply, String Services, String Communications, String PortAuthority, String View, String Observation, List<String> URLIMAGE) {
        this.primaryid = primaryid;
        this.NameOfPort = NameOfPort;
        this.Country = Country;
        this.LatitudeDegree = LatitudeDegree;
        this.LatitudeMinutes = LatitudeMinutes;
        this.LatitudeDecimal = LatitudeDecimal;
        this.LatitudePosition = LatitudePosition;
        this.LongitudeDegree = LongitudeDegree;
        this.LongitudeMinutes = LongitudeMinutes;
        this.LongitudeDecimal = LongitudeDecimal;
        this.LongitudePosition = LongitudePosition;
        this.Anchorages = Anchorages;
        this.Pilotage = Pilotage;
        this.Directions = Directions;
        this.Tug = Tug;
        this.Wharves = Wharves;
        this.CargoHandling = CargoHandling;
        this.RepairAndRescue = RepairAndRescue;
        this.Supply = Supply;
        this.Services = Services;
        this.Communications = Communications;
        this.PortAuthority = PortAuthority;
        this.View = View;
        this.Observation = Observation;
        this.URLIMAGE = URLIMAGE;
    }

    public int getPrimaryid() {
        return primaryid;
    }

    public void setPrimaryid(int primaryid) {
        this.primaryid = primaryid;
    }

    public String getNameOfPort() {
        return NameOfPort;
    }

    public void setNameOfPort(String nameOfPort) {
        NameOfPort = nameOfPort;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }


    public String getLatitudeDegree() {
        return LatitudeDegree;
    }

    public void setLatitudeDegree(String latitudeDegree) {
        LatitudeDegree = latitudeDegree;
    }

    public String getLatitudeMinutes() {
        return LatitudeMinutes;
    }

    public void setLatitudeMinutes(String latitudeMinutes) {
        LatitudeMinutes = latitudeMinutes;
    }

    public String getLatitudeDecimal() {
        return LatitudeDecimal;
    }

    public void setLatitudeDecimal(String latitudeDecimal) {
        LatitudeDecimal = latitudeDecimal;
    }

    public String getLatitudePosition() {
        return LatitudePosition;
    }

    public void setLatitudePosition(String latitudePosition) {
        LatitudePosition = latitudePosition;
    }

    public String getLongitudeDegree() {
        return LongitudeDegree;
    }

    public void setLongitudeDegree(String longitudeDegree) {
        LongitudeDegree = longitudeDegree;
    }

    public String getLongitudeMinutes() {
        return LongitudeMinutes;
    }

    public void setLongitudeMinutes(String longitudeMinutes) {
        LongitudeMinutes = longitudeMinutes;
    }

    public String getLongitudeDecimal() {
        return LongitudeDecimal;
    }

    public void setLongitudeDecimal(String longitudeDecimal) {
        LongitudeDecimal = longitudeDecimal;
    }

    public String getLongitudePosition() {
        return LongitudePosition;
    }

    public void setLongitudePosition(String longitudePosition) {
        LongitudePosition = longitudePosition;
    }

    public String getAnchorages() {
        return Anchorages;
    }

    public void setAnchorages(String anchorages) {
        Anchorages = anchorages;
    }

    public String getPilotage() {
        return Pilotage;
    }

    public void setPilotage(String pilotage) {
        Pilotage = pilotage;
    }

    public String getDirections() {
        return Directions;
    }

    public void setDirections(String directions) {
        Directions = directions;
    }

    public String getTug() {
        return Tug;
    }

    public void setTug(String tug) {
        Tug = tug;
    }

    public String getWharves() {
        return Wharves;
    }

    public void setWharves(String wharves) {
        Wharves = wharves;
    }

    public String getCargoHandling() {
        return CargoHandling;
    }

    public void setCargoHandling(String cargoHandling) {
        CargoHandling = cargoHandling;
    }

    public String getRepairAndRescue() {
        return RepairAndRescue;
    }

    public void setRepairAndRescue(String repairAndRescue) {
        RepairAndRescue = repairAndRescue;
    }

    public String getSupply() {
        return Supply;
    }

    public void setSupply(String supply) {
        Supply = supply;
    }

    public String getServices() {
        return Services;
    }

    public void setServices(String services) {
        Services = services;
    }

    public String getCommunications() {
        return Communications;
    }

    public void setCommunications(String communications) {
        Communications = communications;
    }

    public String getPortAuthority() {
        return PortAuthority;
    }

    public void setPortAuthority(String portAuthority) {
        PortAuthority = portAuthority;
    }

    public String getView() {
        return View;
    }

    public void setView(String view) {
        View = view;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String observation) {
        Observation = observation;
    }

    public List<String> getURLIMAGE() {
        return URLIMAGE;
    }
}
