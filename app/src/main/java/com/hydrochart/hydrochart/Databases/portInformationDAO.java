package com.hydrochart.hydrochart.Databases;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface portInformationDAO {

    @Insert
    long addportInformationDraft(portinformationDraft portDraft);

    @Update
    void updateportinformationDraft(portinformationDraft portDraft);

    @Query("select * from portinformationDraft")
    List<portinformationDraft> getportinformationDraft();

    @Query("select * from portinformationDraft where primaryid==:id")
    List<portinformationDraft> getportinformationDraftById(long id);
}
