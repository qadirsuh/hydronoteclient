package com.hydrochart.hydrochart.Databases;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ENCDAO {

    @Insert
    long addENC(ENCDraft encDraft);

    @Update
    void updateENC(ENCDraft encDraft);

    @Query("select * from ENCDraft")
    List<ENCDraft> getENCDraft();

    @Query("select * from ENCDraft where primaryid==:id")
    List<ENCDraft> getENCDraftById(long id);
}
