package com.hydrochart.hydrochart.Databases;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.List;

@Entity(tableName = "MarineLifeDraft")
public class MarineLifeDraft {

    @PrimaryKey(autoGenerate = false)
    int primaryid;

    @ColumnInfo(name = "Reportingselectposition")
    String Reportingselectposition;

    @ColumnInfo(name = "Species")
    String Species;

    @ColumnInfo(name = "Confidenceselectposition")
    String Confidenceselectposition;

    @ColumnInfo(name = "NameObserved")
    String NameObserved;

    @ColumnInfo(name = "Length")
    String Length;

    @ColumnInfo(name = "Behaviour")
    String Behaviour;

    @ColumnInfo(name = "Date")
    String Date;

    @ColumnInfo(name = "Time")
    String Time;

    @ColumnInfo(name = "LatitudeDegree")
    String LatitudeDegree;

    @ColumnInfo(name = "LatitudeMinutes")
    String LatitudeMinutes;

    @ColumnInfo(name = "LatitudeDecimal")
    String LatitudeDecimal;

    @ColumnInfo(name = "LatitudePosition")
    String LatitudePosition;



    @ColumnInfo(name = "LongitudeDegree")
    String LongitudeDegree;

    @ColumnInfo(name = "LongitudeMinutes")
    String LongitudeMinutes;

    @ColumnInfo(name = "LongitudeDecimal")
    String LongitudeDecimal;

    @ColumnInfo(name = "LongitudePosition")
    String LongitudePosition;

    @ColumnInfo(name = "Monitoring")
    String Monitoring;

    @ColumnInfo(name = "Meteorological")
    String Meteorological;

    @ColumnInfo(name = "RangePosition")
    String RangePosition;

    @ColumnInfo(name = "Bearing")
    String Bearing;

    @ColumnInfo(name = "Observation")
    String Observation;

    @TypeConverters(GithubTypeConverters.class)
    public final List<String> URLIMAGE;


    public MarineLifeDraft(int primaryid, String Reportingselectposition, String Species, String Confidenceselectposition, String NameObserved, String Length, String Behaviour, String Date, String Time, String LatitudeDegree, String LatitudeMinutes, String LatitudeDecimal, String LatitudePosition, String LongitudeDegree, String LongitudeMinutes, String LongitudeDecimal, String LongitudePosition, String Monitoring, String Meteorological, String RangePosition, String Bearing, String Observation, List<String> URLIMAGE) {
        this.primaryid = primaryid;
        this.Reportingselectposition = Reportingselectposition;
        this.Species = Species;
        this.Confidenceselectposition = Confidenceselectposition;
        this.NameObserved = NameObserved;
        this.Length = Length;
        this.Behaviour = Behaviour;
        this.Date = Date;
        this.Time = Time;
        this.LatitudeDegree = LatitudeDegree;
        this.LatitudeMinutes = LatitudeMinutes;
        this.LatitudeDecimal = LatitudeDecimal;
        this.LatitudePosition = LatitudePosition;
        this.LongitudeDegree = LongitudeDegree;
        this.LongitudeMinutes = LongitudeMinutes;
        this.LongitudeDecimal = LongitudeDecimal;
        this.LongitudePosition = LongitudePosition;
        this.Monitoring = Monitoring;
        this.Meteorological = Meteorological;
        this.RangePosition = RangePosition;
        this.Bearing = Bearing;
        this.Observation = Observation;
        this.URLIMAGE = URLIMAGE;
    }


    public int getPrimaryid() {
        return primaryid;
    }

    public void setPrimaryid(int primaryid) {
        this.primaryid = primaryid;
    }

    public String getReportingselectposition() {
        return Reportingselectposition;
    }

    public void setReportingselectposition(String reportingselectposition) {
        Reportingselectposition = reportingselectposition;
    }

    public String getSpecies() {
        return Species;
    }

    public void setSpecies(String species) {
        Species = species;
    }

    public String getConfidenceselectposition() {
        return Confidenceselectposition;
    }

    public void setConfidenceselectposition(String confidenceselectposition) {
        Confidenceselectposition = confidenceselectposition;
    }

    public String getNameObserved() {
        return NameObserved;
    }

    public void setNameObserved(String nameObserved) {
        NameObserved = nameObserved;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getBehaviour() {
        return Behaviour;
    }

    public void setBehaviour(String behaviour) {
        Behaviour = behaviour;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getLatitudeDegree() {
        return LatitudeDegree;
    }

    public void setLatitudeDegree(String latitudeDegree) {
        LatitudeDegree = latitudeDegree;
    }

    public String getLatitudeMinutes() {
        return LatitudeMinutes;
    }

    public void setLatitudeMinutes(String latitudeMinutes) {
        LatitudeMinutes = latitudeMinutes;
    }

    public String getLatitudeDecimal() {
        return LatitudeDecimal;
    }

    public void setLatitudeDecimal(String latitudeDecimal) {
        LatitudeDecimal = latitudeDecimal;
    }

    public String getLatitudePosition() {
        return LatitudePosition;
    }

    public void setLatitudePosition(String latitudePosition) {
        LatitudePosition = latitudePosition;
    }

    public String getLongitudeDegree() {
        return LongitudeDegree;
    }

    public void setLongitudeDegree(String longitudeDegree) {
        LongitudeDegree = longitudeDegree;
    }

    public String getLongitudeMinutes() {
        return LongitudeMinutes;
    }

    public void setLongitudeMinutes(String longitudeMinutes) {
        LongitudeMinutes = longitudeMinutes;
    }

    public String getLongitudeDecimal() {
        return LongitudeDecimal;
    }

    public void setLongitudeDecimal(String longitudeDecimal) {
        LongitudeDecimal = longitudeDecimal;
    }

    public String getLongitudePosition() {
        return LongitudePosition;
    }

    public void setLongitudePosition(String longitudePosition) {
        LongitudePosition = longitudePosition;
    }

    public String getMonitoring() {
        return Monitoring;
    }

    public void setMonitoring(String monitoring) {
        Monitoring = monitoring;
    }

    public String getMeteorological() {
        return Meteorological;
    }

    public void setMeteorological(String meteorological) {
        Meteorological = meteorological;
    }

    public String getRangePosition() {
        return RangePosition;
    }

    public void setRangePosition(String rangePosition) {
        RangePosition = rangePosition;
    }

    public String getBearing() {
        return Bearing;
    }

    public void setBearing(String bearing) {
        Bearing = bearing;
    }

    public String getObservation() {
        return Observation;
    }

    public void setObservation(String observation) {
        Observation = observation;
    }

    public List<String> getURLIMAGE() {
        return URLIMAGE;
    }
}
