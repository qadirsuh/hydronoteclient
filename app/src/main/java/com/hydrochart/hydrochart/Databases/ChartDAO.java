package com.hydrochart.hydrochart.Databases;

import androidx.room.Dao;
import androidx.room.Database;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ChartDAO {

    @Insert
    long addChartDraft(ChartDraft chartDraft);

    @Update
    void updateDetails(ChartDraft chartDraft);

    @Query("select * from ChartDraft")
    List<ChartDraft> getChartDraft();

    @Query("select * from ChartDraft where primaryid==:id")
    List<ChartDraft> getChartDraftById(long id);
}
