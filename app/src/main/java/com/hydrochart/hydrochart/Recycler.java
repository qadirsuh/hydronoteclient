package com.hydrochart.hydrochart;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Recycler extends RecyclerView.Adapter<Recycler.ViewHolder> {

    Context context;
    List<String> img;


    public Recycler(Context context, List<String> img) {
        this.context = context;
        this.img = img;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.imgrow, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return  viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.imgs.setImageURI(Uri.parse(img.get(position)));
    }

    @Override
    public int getItemCount() {
        return img.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView imgs;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            imgs = itemView.findViewById(R.id.gridIMG);
        }
    }
}
